##### Stage 1
FROM registry.adnerp.net/node:latest as node
WORKDIR /app
COPY package.json package.json
COPY . .
RUN npm install
RUN npm run build -- --prod && npm run post-build

##### Stage 2
FROM registry.adnerp.net/nginx:latest
VOLUME /var/cache/nginx
ENV SET_CONTAINER_TIMEZONE=true
ENV CONTAINER_TIMEZONE=America/Bogota
ENV TZ=America/Bogota
COPY --from=node /app/dist/AdminPanel /usr/share/nginx/html
COPY ./config/nginx.conf /etc/nginx/conf.d/default.conf 


# docker build -t spa_adminv5 -f nginx.prod.dockerfile .
# docker run -p 8080:80 spa_adn
# docker run -p 80:80 -v "$(pwd)/dist/adn":/usr/share/nginx/html spa_adn