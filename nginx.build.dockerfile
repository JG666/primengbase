##### Stage 1
FROM registry.adnerp.net/node:latest as node
WORKDIR /app
COPY package.json package.json
COPY . .
RUN npm install
RUN npm run build -- --prod

# ##### Stage 1
# FROM registry.adnerp.net/node:latest as node
# WORKDIR /app
# ENV SET_CONTAINER_TIMEZONE=true
# ENV CONTAINER_TIMEZONE=America/Bogota
# ENV TZ=America/Panama
# COPY package.json package.json
# COPY . .
# RUN npm install
# # RUN apk add --no-cache tzdata
# RUN npm run build -- --prod

# ##### Stage 2
# FROM registry.adnerp.net/nginx:latest
# VOLUME /var/cache/nginx
# ENV SET_CONTAINER_TIMEZONE=true
# ENV CONTAINER_TIMEZONE=America/Panama
# ENV TZ=America/Panama
# COPY --from=node /app/dist/AdminPanel /usr/share/nginx/html
# COPY ./config/nginx.conf /etc/nginx/conf.d/default.conf 


# # docker build -t spa_adnv4 -f nginx.prod.dockerfile .
# # docker run -p 8080:80 spa_adn
# # docker run -p 80:80 -v "$(pwd)/dist/adn":/usr/share/nginx/html spa_adn


# # arguments

# # --build-arg CI_VRSN=$(Build.SourceVersion)

# ##### Stage 1
# FROM registry.adnerp.net/node:latest as node
# WORKDIR /app
# COPY package.json package.json
# COPY . .
# RUN npm install
# # RUN sed -ri "s|\"VERSION\"|\"MY_DEVOPS_VRSN\"|" src/environments/environment.prod.ts
# RUN npm run build --prod && npm run post-build
# # RUN npm run build -- --prod

# ##### Stage 2
# FROM getsentry/sentry-cli
# ARG CI_VRSN
# WORKDIR /workmaps
# # COPY --from=node /app/dist/AdminPanel /workmaps/dist/AdminPanel
# COPY --from=node /app/.git /workmaps
# COPY --from=node /app/dist/AdminPanel /workmaps/dist/AdminPanel
# RUN echo "Create a new release $CI_VRSN" && \
#     #- export SENTRY_URL=$SENTRY_BASE_URL
#     # export SORT_CI_ID=${CI_VRSN:0:8} && \
#     export SENTRY_AUTH_TOKEN="ad69d334bf5740859cebd4318f473197fe960fe021e943f8a14090898f4d159c"  && \
#     export SENTRY_ORG="microservices" && \
#     export SENTRY_PROJECT="adminpanel" && \
#     sentry-cli releases new $CI_VRSN && \
#     sentry-cli releases set-commits --auto $CI_VRSN && \
#     sentry-cli releases files $CI_VRSN upload-sourcemaps /workmaps/dist/AdminPanel -x .js -x .map --validate --verbose --rewrite --strip-common-prefix && \
#     sentry-cli releases finalize $CI_VRSN 
    

# ##### Stage 3
# FROM registry.adnerp.net/nginx:latest
# VOLUME /var/cache/nginx
# ENV SET_CONTAINER_TIMEZONE=true
# ENV CONTAINER_TIMEZONE=America/Bogota
# ENV TZ=America/Bogota
# COPY --from=node /app/dist/AdminPanel /usr/share/nginx/html
# RUN rm /usr/share/nginx/html/*.map
# COPY ./config/nginx.conf /etc/nginx/conf.d/default.conf 


# # docker build -t admin_panelv1 -f nginx.prod.dockerfile .
# # docker run -p 8080:80 spa_admin