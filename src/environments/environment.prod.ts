export const environment = {
  production: true,
  version: "{{ENV_VERSION}}",
  base_url: "http://excelssisdb-env.eba-ixwmibmm.us-east-1.elasticbeanstalk.com:8081"
};
