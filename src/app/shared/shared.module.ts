import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimegnModule } from '../common/primegn/primegn.module';
import { TruncatePipe } from './pipes/truncate.pipe';
import { HrefPreventDefaultDirective } from './directive/href-prevent-default.directive';
import { HasClaimDirective } from './directive/has-claim.directive';
import { AuthPage } from './directive/auth-page.directive';
import { DatesPipe } from './pipes/dates.pipe';
import { MessageModule } from 'primeng/message';
import { ConfirmationService } from 'primeng/api';


@NgModule({
  imports: [
    CommonModule,
    PrimegnModule,
    MessageModule,
  ],
  declarations: [
        TruncatePipe, 
        HrefPreventDefaultDirective,
        HasClaimDirective,
        AuthPage,
        DatesPipe
  ],
  exports : [
    PrimegnModule,
    TruncatePipe,
    HrefPreventDefaultDirective,
    HasClaimDirective,
    AuthPage,
    MessageModule,
    DatesPipe,
  ],
  providers: [ConfirmationService]
})
export class SharedModule { }
