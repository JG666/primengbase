import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
moment.locale('es');

@Pipe({ name: 'momentPipe' })
export class DatesPipe implements PipeTransform {
    transform(value: Date, dateFormat: string, invalid ?: boolean): any {
        return moment(value).format(dateFormat);
    }
}
