import { MessageService } from 'primeng/api';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginComponent } from './login/login.component';
import { PrimegnModule } from '../common/primegn/primegn.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
//import { MsalGuard } from '@azure/msal-angular'; 

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent/*,  
    canActivate: [MsalGuard] */
  }
];

@NgModule({
  imports: [
    CommonModule,
    PrimegnModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    LoginComponent
  ],
  exports : [LoginComponent],
  providers: [MessageService],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]

})
export class AuthModule { }
