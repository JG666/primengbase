import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/services/Auth/auth.service';
import { Router } from '@angular/router';

import {MessageService} from 'primeng/api';
import { SubSink } from 'subsink';
import { debounceTime} from 'rxjs/operators';
import { environment } from 'src/environments/environment';


export const EmailValidation = [
  Validators.required,
  // Validators.email,
  Validators.maxLength(50),
];
export const PasswordValidation = [
  Validators.required,
  Validators.minLength(8),
  Validators.maxLength(50),
];

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
  providers: [MessageService],
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  loginError = "";
  subSink = new SubSink();
  isLoading : boolean = false;
  redirectUrl;
  sttcFiles:any[];
  urlBackImg:string =  `../../../assets/custom/images/default-bg2.jpg`;
  urlLogoImg:string =  `../../../assets/custom/images/logoicbf.png`;

  constructor(private fb: FormBuilder, private authService: AuthService,
        private router: Router, private toastService: MessageService ) {
        // route.paramMap.subscribe(
        //   params => (this.redirectUrl = params.get('redirectUrl'))
        // );
      }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ["", EmailValidation],
      password: ["", PasswordValidation],
    });  
  }

  SignInUser(subForm: FormGroup) {    
    let xUsrCrd = {
      username: (subForm.value.username).trim(),
      password: subForm.value.password,
    };
    this.isLoading = true;
    this.subSink.sink = this.authService
      .SignIn(xUsrCrd)
      .pipe(
        debounceTime(5000)
      ).subscribe( dt => { 
        if (!dt) return;
          this.loginForm.reset();
          this.isLoading = false;
          this.router.navigateByUrl('/inicio');
          // this.router.navigate(['/home']);
        },
        (err) => {
          this.isLoading = false;
          this.toastService.add({
            key:"tst-lgn", severity: "warn",
            summary: "Error Login",
            detail: err, sticky:true, closable: true });
        }
      );
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}

