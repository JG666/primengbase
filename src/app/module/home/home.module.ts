import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/services/Auth/auth-guard.service';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '',canActivate: [AuthGuardService], component: HomeComponent }])
  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
