import { environment } from 'src/environments/environment';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { HttpClient } from '@angular/common/http';
import { VersionService } from './services/version.service';
import { take } from 'rxjs/operators';
import { LocalStorageService } from './services/Auth/local-storage.service';
import { AuthService } from './services/Auth/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})
export class AppComponent  implements OnInit, OnDestroy {
    // npm run build --prod  && npm run post-build

    title = 'Admin Panel';
    subSink = new SubSink();
    private currentHash = '{{POST_BUILD_ENTERS_HASH_HERE}}';
    private currentVersion = false;

    constructor(private http: HttpClient, private versionService: VersionService,
      private localStorageService:LocalStorageService, private loginService : AuthService) {
    }
  
    private initVersionCheck(url) {
      setInterval(() => {
        this.checkVersion(url);
      }, 30000);
    }
  
    private checkVersion(url) { //TODO: build a web worker to handle background
      // timestamp these requests to invalidate caches
      // console.log('checkVersion get ***', url);
      this.subSink.sink = this.http
        .get(url + '?t=' + new Date().getTime(), { headers: { ignoreLoadingBar: '' } })
        .pipe(take(1))
        .subscribe(
          (res: any) => {
            const hash = res.hash;
            const hashChanged = this.hasHashChanged(this.currentHash, hash, res.date);
            if (hashChanged) {
              console.log('current version  : ', this.currentHash);
              console.log('new version : ', hash);
              this.versionService.setVersionDate(res.date);
              this.versionService.setupToUpDate(false);
              if (confirm('Nueva version disponible. Desea actualizar?')) {
                window.location.reload(true);
              }
            }
            // store the new hash so we wouldn't trigger versionChange again
            // only necessary in case you did not force refresh
            this.currentHash = hash;
          },
          (err) => {
            console.error(err, 'Could not get version');
          }
        );
    }
  
    private hasHashChanged(currentHash, newHash, date) {
      if (!currentHash || currentHash === '{{POST_BUILD_ENTERS_HASH_HERE}}') {
        return false;
      }
      if (!this.currentVersion) {
        this.currentVersion = true;
        this.versionService.setVersionDate(date);
      }
      return currentHash !== newHash;
    }
  
    ngOnInit() {
      const userLocal = this.localStorageService.getLocalValue('usrLgndt'); // localStorage.getItem('userData');
      if(!!userLocal) { 
        this.loginService.initStoreFromLocal(JSON.parse(userLocal));
      }else { this.loginService.InitAuthStoreState(); }
      
      this.versionService.initStore('?$%&xYz123abc$%');
      if (environment.production) {
        this.initVersionCheck(`/version.json`);
      }

    }

    ngOnDestroy(): void {
        this.subSink.unsubscribe();
      }
}
