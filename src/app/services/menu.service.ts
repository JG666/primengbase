import { Injectable } from '@angular/core';
import { ObservableStore } from '@codewithdan/observable-store';
import { MenuStates, MenuActions } from '../common/store/store-states';

@Injectable({
  providedIn: 'root'
})
export class MenuService  extends ObservableStore<MenuStates> {
  
  constructor() {
        super(
          { stateSliceSelector: state => {
              return {
                displayModules: (state !== null ) ? state.displayModules : false,
                menuMode: (state !== null ) ? state.menuMode : 'static',
                lightMenu: (state !== null ) ? state.lightMenu : null,
                overlayMenuActive: (state !== null ) ? state.overlayMenuActive : false,
                staticMenuDesktopInactive: (state !== null ) ? state.staticMenuDesktopInactive : false,
                staticMenuMobileActive: (state !== null ) ? state.staticMenuMobileActive : false,
                topbarColor: (state !== null ) ? state.topbarColor : false,
                resetMenu: (state !== null ) ? state.resetMenu : false,
                generalMenu: (state !== null ) ? state.generalMenu : null,
                menuItems: (state !== null ) ? state.menuItems : null,
                rightMenuClick: (state !== null ) ? state.rightMenuClick : false,
                onMenuButtonClick: (state !== null ) ? state.onMenuButtonClick : null
              };
            }
        });
        this.InitMenuStoreState();
    }



    private dtTranform(dt) {
        let arr = dt.slice();
        return arr.reduce((z,a,i) => { //TODO: feature routing must have prefixed module
          let x = !!z && z.slice().find( y => y.md_link == a.md_link );
          if (!!x)   //module already within the object map.
            z.map( y => (y.md_link == a.md_link) ? y['items'].push({ ft_title: a.ft_title, ft_icon: `${a.ft_icon}`, routerLink: [`/${a.md_link}/${a.ft_link}`], feature: a.feature }) : y )
         else z.push({md_link: a.md_link, md_title: a.md_title, md_icon: `${a.md_icon}`,  module: `${a.module}`, // routerLink:[`/${a.md_link}`]
                      items:[{ ft_title: a.ft_title, ft_icon: `${a.ft_icon}`, routerLink: [`/${a.md_link}/${a.ft_link}`], feature: a.feature }]}); 
          return z;            
        },[]);
      }


    setMenuOp(op, dt, wh?): any {
        switch (op) {
            case 'displayModules':
                this.setState({ displayModules: dt }, MenuActions.SetOp);
                break;
            case 'menuMode':
                this.setState({ menuMode: dt }, MenuActions.SetOp);
                break;
            case 'lightMenu':
                this.setState({ lightMenu: dt }, MenuActions.SetOp);
                break;
            case 'overlayMenuActive':
                this.setState({ overlayMenuActive: dt }, MenuActions.SetOp+'~'+wh);
                break;
            case 'staticMenuDesktopInactive':
                this.setState({ staticMenuDesktopInactive: dt }, MenuActions.SetOp);
                break;
            case 'staticMenuMobileActive':
                this.setState({ staticMenuMobileActive: dt }, MenuActions.SetOp);
                break;
            case 'topbarColor':
                this.setState({ topbarColor: dt }, MenuActions.SetOp);
                break;
            case 'resetMenu':
                this.setState({ resetMenu: dt }, MenuActions.SetOp+'~'+wh);
                break;
            case 'generalMenu':
                this.setState({ generalMenu: dt }, MenuActions.SetOp);
                break;
            case 'rightMenuClick':
                this.setState({ rightMenuClick: dt }, MenuActions.SetOp);
                break;
            case 'onMenuButtonClick':
                this.setState({ onMenuButtonClick: dt }, MenuActions.SetOp+'~'+wh);
                break;
            case 'menuItems':                
                let xMenu = this.dtTranform(dt);
                xMenu.unshift({ module : 'Modulos' , md_title: 'Modulos', md_icon: 'home', md_link: 'modulos'}); //, routerLink : ['/inicio']})
                const tmp = {
                    items: [
                        {
                            feature: "mision",
                            ft_icon: "style",
                            ft_title: "Misión",
                            routerLink: ["/md-estructuraacademica/ft-mision"]
                        },
                        {
                            feature: "vision",
                            ft_icon: "style",
                            ft_title: "Visión",
                            routerLink: ["/md-estructuraacademica/ft-vision"]
                        },
                        {
                            feature: "planacademico",
                            ft_icon: "style",
                            ft_title: "Plan Academico",
                            routerLink: ["/md-estructuraacademica/ft-planacademico"]
                        },
                        {
                            feature: "departamento",
                            ft_icon: "style",
                            ft_title: "Departamento",
                            routerLink: ["/md-estructuraacademica/ft-departamento"]
                        },
                        {
                            feature: "lineaaccion",
                            ft_icon: "style",
                            ft_title: "Línea Acción",
                            routerLink: ["/md-estructuraacademica/ft-lineaaccion"]
                        },
                        {
                            feature: "programaacademico",
                            ft_icon: "style",
                            ft_title: "Programa Academico",
                            routerLink: ["/md-estructuraacademica/ft-programaacademico"]
                        },
                    ],
                    md_icon: "dynamic_form",
                    md_link: "md-estructuraacademica",
                    md_title: "Estructura Academica",
                    module: "estructuraacademica"
                };
                xMenu.push(tmp);
                this.setState({ menuItems: xMenu }, MenuActions.SetMenu+'~'+wh);
            break;
        }

    }

      getMenuOp(op): any {
            switch (op) {
                case 'displayModules':
                    return this.getState().displayModules;
                case 'menuMode':
                    return this.getState().menuMode;
                case 'lightMenu':
                    return this.getState().lightMenu;
                case 'overlayMenuActive':
                    return this.getState().overlayMenuActive;
                case 'staticMenuDesktopInactive':
                    return this.getState().staticMenuDesktopInactive;
                case 'staticMenuMobileActive':
                    return this.getState().staticMenuMobileActive;
                case 'topbarColor':
                    return this.getState().topbarColor;
                case 'resetMenu':
                    return this.getState().resetMenu;
                case 'generalMenu':
                    return this.getState().generalMenu;
                case 'menuItems':
                    return this.getState().generalMenu;
                case 'rightMenuClick':
                    return this.getState().rightMenuClick;
                case 'onMenuButtonClick':
                    return this.getState().onMenuButtonClick;
            }
        }

    InitMenuStoreState(dt?,wh?): void {
        const initialState = {
            displayModules : false,
            menuMode : 'static',
            lightMenu : null,
            overlayMenuActive : true,
            staticMenuMobileActive : null,
            staticMenuDesktopInactive : null,
            topbarColor : null,
            resetMenu : false,
            menuItems : null,
            rightMenuClick : false,
            onMenuButtonClick : null
        };
        this.setState(initialState, MenuActions.InitialState+'~'+wh);
      }

}

