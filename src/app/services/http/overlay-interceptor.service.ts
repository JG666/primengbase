import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';

import { Observable } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';

import { OverlayService } from '../overlay.service';
import { DropResquestService } from './drop-request.service';

@Injectable({ providedIn: 'root' })
export class OverlayLoaderInterceptor implements HttpInterceptor {

    constructor(public overlayService: OverlayService, private dropResquest: DropResquestService ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {  
    // console.log("intercepting req ***",req.url, !req.url.includes('/version.json'), !req.url.includes('/cdn'));  
    if(!req.url.includes('/version.json') && !req.url.includes('/cdn')) 
      this.overlayService.show();
    return next.handle(req).pipe(
    //   takeUntil(this.dropResquest.onCancelPendingRequests()),
      finalize(() => this.overlayService.hide())
    );
  }
  
}
