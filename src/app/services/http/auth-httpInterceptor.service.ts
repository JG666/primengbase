import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpErrorResponse,
} from '@angular/common/http';
import { Injectable, Injector, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError} from 'rxjs/operators';
import { AuthService } from '../Auth/auth.service';
import { SubSink } from 'subsink';

@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor, OnDestroy  {
  
  private token :string ;
  private isLogIn  = false ;
  private subSink = new SubSink();

    // private authService: AuthService, private router: Router,private http: HttpClient
  constructor(private injector: Injector) {
    this.subSink.sink = this.authService.stateWithPropertyChanges.subscribe((state) => {
        if (!!state && state.stateChanges.hasOwnProperty('authStatus')) 
          this.isLogIn = state.stateChanges.authStatus || false;
        if (!!state && state.stateChanges.hasOwnProperty('token')) 
          this.token = state.stateChanges.token || null;
      });
      this.token = this.authService.GetStateLoginOp('token') || null;
      this.isLogIn = this.authService.GetStateLoginOp('authStatus') || false;
  }
  private authService = this.injector.get(AuthService);
  private router = this.injector.get(Router);

  intercept(req: HttpRequest<any>,next: HttpHandler): Observable<HttpEvent<any>> {
    let authRequest: HttpRequest<any>;
    if (this.isLogIn) {
      authRequest = req.clone({ setHeaders: { authorization: `${this.token}` } });
    } else {
      authRequest = req.clone();
    }
    return next.handle(authRequest).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err && err.status === 401) {
          this.JustLogOut(err,'no tkn-expired');
        }
        return observableThrowError(err);
      })
    );
  }
  
 private JustLogOut(err,wr) {
   if(err  && (err.status == 401 || err.status == 422)){
     this.authService.SignOut();
     this.router.navigate(['/login'], { queryParams: {redirectUrl: this.router.routerState.snapshot.url || '' } });
   }
    return observableThrowError(err); 
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
