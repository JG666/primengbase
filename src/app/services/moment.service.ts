import * as moment from 'moment-timezone';
import { Injectable } from '@angular/core';
moment.locale('es');
moment.tz.setDefault("America/Bogota");

@Injectable({
    providedIn: 'root'
})
export class MomentDateService {

    getUtcNow(): any {
         return moment().utc().tz('America/Bogota').format();
    }

    getUtcFormated(value: Date, dateFormat: string): any {
        return moment(value).utc().tz('America/Bogota').format(dateFormat);
    }

    getMomentBetween(start, end, date): any {
        // date = moment(date).unix();
        // start = moment(start).unix();
        // end = moment(end).unix();
        // console.log('moment between **', (date >= start && date <= end),start, end, date);
        // return (date >= start && date <= end) ;
        return moment(date).isBetween(start,end);
    }

    getMomentCmd( cmd:string, date?:Date, oprtr?:string): any {
        switch (cmd) {
            case 'startof':
                return moment().utc().tz('America/Bogota').startOf('day');
            case 'endof':
                return moment().utc().tz('America/Bogota').endOf('day');
        
            default:
                return moment();
        }
    }

}
