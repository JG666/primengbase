import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

import { throwError, Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: "root",
})
export class TestService {
    
  constructor(private http: HttpClient) { }

  getNtfTesting(op:string): Observable<any> { // claim:scring ~ module@feature  ex: salesMachineryCrm@feature
    // const params = new HttpParams().set("option",op);
    var headers = new HttpHeaders().set("Content-Type", "application/json",);
    // httpClient.get('/status', { headers: { ignoreLoadingBar: '' } }); // ignore a particular $http GET loading bar:
    return this.http
      .post<any>(`${environment.base_url}/api/email-sms/${op}`,{headers})
      .pipe(
        map(dt => {
          if(dt['type_msg']== "push")        
          return dt;
        }),
        catchError(this.handleError)
      );
  }

  getNotifyTest(url): Observable<any> { // claim:scring ~ module@feature  ex: salesMachineryCrm@feature
    var headers = new HttpHeaders().set("Content-Type", "application/json",);
    // httpClient.get('/status', { headers: { ignoreLoadingBar: '' } }); // ignore a particular $http GET loading bar:
    return this.http
      .post<any>(`${environment.base_url}/api/${url}`,{headers})
      .pipe(
        map(dt => dt),
        catchError(this.handleError)
      );
  }

  getAuthTest(url, hdr?, claim?): Observable<any> { // claim:scring ~ module@feature  ex: salesMachineryCrm@feature
    var headers = new HttpHeaders().set("Content-Type", "application/json",);
    if(hdr) {
         headers =  new HttpHeaders({
            'Content-Type': 'application/json',
            'custom-access': claim });
        }
    // httpClient.get('/status', { headers: { ignoreLoadingBar: '' } }); // ignore a particular $http GET loading bar:
    console.log(`prms *** url : ${url} , claim : ${claim}`);
    return this.http
      .get<any>(`${environment.base_url}/api/${url}`,{headers})
      .pipe(
        map(dt => dt),
        catchError(this.handleError)
      );
  }

  private handleError(err) {
    console.log('handleError ***',err);
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${JSON.stringify((err.error && err.error.Message)|| err.message)}`;
    } else {
      // The backend returned an unsuccessful response code. The response body may contain clues as to what went wrong,
      errorMessage = `Backend returned error: ${JSON.stringify((err.error && err.error.Message) || err.message)}`;
    }
    return throwError(errorMessage);
  }
}


// switch (op) {
//   case "userLogin":
//     return this.getState().userLogin;
//   case "menu":
//   case "authStatus":
//     return this.getState().authStatus;
//   case "refreshToken":
//     return this.getState().refreshToken;
// }