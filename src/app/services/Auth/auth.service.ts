import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

import { throwError, Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { ObservableStore } from '@codewithdan/observable-store';
import { AuthStoreState, AuthStoreActions } from '../../common/store/store-states';

import { environment } from '../../../environments/environment';
import { UserLoginModel } from '../../auth/models/auth.model';
import { MenuService } from 'src/app/services/menu.service';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: "root",
})
export class AuthService extends ObservableStore<AuthStoreState> {
  
  constructor(
        private http: HttpClient,  private localStorageService: LocalStorageService,
        private router: Router, private menuService: MenuService,  ) {
    super({ stateSliceSelector: (state) => {
        return {
          userLogin: state !== null ? state.userLogin : null,
          menu: state !== null ? state.menu : null,
          token: state !== null ? state.token : null,
          refreshToken: state !== null ? state.refreshToken : null,
          authStatus: state !== null ? state.authStatus : null,
        };
      },
    });
  }

  SignOut() {
    // TODO: setUp the BackEnd endPoint
    this.localStorageService.removeItem("usrLgndt");
    this.InitAuthStoreState();
    this.SetStoreSignIn(null);
    // if (environment.production) {this.socketService.disconnec
    // this.router.navigateByUrl('/login');
  }

  hasClaim(claimType: any, claimValue?: any) {
    // Usage :   *hasClaim="'featureName'"
    let claims = this.GetStateLoginOp("userLogin");
    let xT = claims.urole;
    return xT !== "Usuario";
  }

  SignIn2(credentials): Observable<any> {
    const rslt = this.processObjMdl(newDtTmp); //user/login
    this.SetStoreSignIn(rslt);
    this.localStorageService.setItem("usrLgndt", rslt);
    this.menuService.setMenuOp("menuItems",rslt['objModel']['user_claims'],"AuthService::fetchUserLoginData");
    return of(rslt);         
  }

  SignIn(credentials): Observable<UserLoginModel> {
    let formData = new FormData();
    formData.append("user", credentials.username);
    formData.append("password", credentials.password);
    return this.http
      .post<any>(`${environment.base_url}/excelsis-api/user/login`, formData)
      .pipe(
        tap(dt => {
          if (dt["codError"] !== "200") {
            this.router.navigate(["/login"]);
            return false;
          } else {
            const rslt = this.processObjMdl(dt); //user/login newDtTmp
            this.SetStoreSignIn(rslt);
            this.localStorageService.setItem("usrLgndt", rslt);
            this.menuService.setMenuOp("menuItems",rslt.objModel.user_claims,"AuthService::fetchUserLoginData");
            return rslt ;
          }
        }),
        catchError(this.handleError)
      );
  }

  processObjMdl(dt) {
    const xVl = dt['user']['UserRoles'][0]['rolId']['AccessRoles'][0]['accessId'];
    const i = xVl['AccessComponents'].find(x=> x['componentsId']['type'] == 'MODULO');
    let objModel = [];
      for(var j of xVl['AccessComponents']) {
        if( j['componentsId']['type'] == 'SUBMODULO'){
          objModel.push({
            md_id: i['componentsId']['id'] ,
            module: (i['componentsId']['name']).replace(" ","").toLowerCase(),
            md_title: i['componentsId']['name'],
            md_link: 'md-'+(i['componentsId']['name']).replace(" ","").toLowerCase(),
            md_icon: 'dynamic_form', 
            ft_id: j['componentsId']['id'],
            feature: (j['componentsId']['name']).replace(" ","").toLowerCase(), 
            ft_title: j['componentsId']['name'],
            ft_link: 'ft-'+(j['componentsId']['name']).replace(" ","").toLowerCase(),
            ft_icon: 'style',
            can_access: true,
            can_edit: true,
          });
        }
      }

      return {
        token: dt['token'],
        refreshToken: 'elrefreshToken',
        success: true,
        objModel: {
           id: dt['user']['id'],
           id_usuario: dt['user']['id'],
           urole: dt['user']['UserRoles'][0]['rolId']['name'],
           perfil: dt['user']['UserRoles'][0]['rolId']['description'],
           usuario_nombre: dt['user']['id'],
           alias: dt['user']['nickName'],
           nombre_completo: dt['user']['name'],
           profesion: dt['user']['name'],
           identificacion: dt['user']['id'],
           genero: dt['user']['name'],
           user_claims:objModel
         }
      };
  }

  private SetStoreSignIn(dt): void {
    this.setState(
      { userLogin: (!!dt && dt.objModel) || null },
      `${AuthStoreActions.UserLogin}`
    );
    this.setState(
      { token: (!!dt && dt.token) || null },
      `${AuthStoreActions.Token}`
    );
    this.setState(
      { refreshToken: (!!dt && dt.refreshToken) || null },
      `${AuthStoreActions.ResfeshToken}`
    );
    this.setState(
      { authStatus: (!!dt && dt.success) || null },
      `${AuthStoreActions.AuthStatus}`
    );
  }

  SetStateLoginOp(op, dt, wh): any {
    // wh => component::method
    if (wh == "401") console.log("just 401 ***", wh);
    switch (op) {
      case "userLogin":
        this.setState({ userLogin: dt }, `${AuthStoreActions.UserLogin}~${wh}`);
        break;
      case "token":
        this.setState({ token: dt }, `${AuthStoreActions.Token}~${wh}`);
        break;
      case "authStatus":
        this.setState(
          { authStatus: dt },
          `${AuthStoreActions.AuthStatus}~${wh}`
        );
        break;
      case "refreshToken":
        this.setState(
          { refreshToken: dt },
          `${AuthStoreActions.ResfeshToken}~${wh}`
        );
        break;
    }
  }

  GetStateLoginOp(op, dt?, wh?): any {
    switch (op) {
      case "userClaims":
        return this.getState().userLogin;
      case "userLogin":
        return this.getState().userLogin;
      case "authStatus":
        let xSt = this.getState().authStatus;
        if (xSt != null) return xSt;
        else {
          let dt = this.localStorageService.getItem("usrLgndt");
          if (!dt) return false;
          this.SetStoreSignIn(dt);
          this.menuService.setMenuOp(
            "menuItems",
            dt.objModel.user_claims,
            "AuthService::GetStateLoginOp"
          );
          return dt.success;
        }
        break;
      case "refreshToken":
        return this.getState().refreshToken;
      case 'token':
        return this.getState().token;
    }
  }

  InitAuthStoreState(op?, wh?): void {
    const initialState = {
      userLogin: null,
      menu: null,
      token: null,
      refreshToken: null,
    };
    this.setState(initialState, `${AuthStoreActions.InitAuthStore}~${wh}`);
  }

  initStoreFromLocal(dt){
    this.SetStoreSignIn(dt);
    this.menuService.setMenuOp("menuItems",dt.objModel.user_claims,"AuthService::initStoreFromLocal");
  }

  private handleError(err) {
    console.log("handleError AuthService ***", err);
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${JSON.stringify(
        (err.error && err.error.Message) || err.message
      )}`;
    } else {
      // The backend returned an unsuccessful response code. The response body may contain clues as to what went wrong,
      errorMessage = `Backend returned error: ${JSON.stringify(
        (err.error && err.error.Message) || err.message
      )}`;
    }
    return throwError(errorMessage);
  }
}


export const dtTmp = {
  token: 'eltoken',
  refreshToken: 'elrefreshToken',
  success: true,
  objModel: {
    id: '22a0b494-2224-404e-9456-0ba8eea8309b',
    id_usuario: '664899b0-bba8-4c4f-aebf-f9d826d98c88',
    urole: 'User',
    perfil: 'Sales Machinery',
    usuario_nombre: 'jg@hot.com',
    alias: 'pashenkov',
    nombre_completo: 'Javier Gonzalez Brieva',
    profesion: 'Electronic Engineer',
    identificacion: '568732432',
    genero: 'male',
    user_data: [
      {
        email_institucional: 'synkronus@support.com',
        email_personal: 'javgonz@gmail.com',
        telefono_institucional: '318 12932 2342',
        telefono_movil: '23847239847',
        direccion_fisica: 'Street 43',
      },
      {
        email_institucional: 'synkronus@support.com',
        email_personal: 'javierg89@gmail.com',
        telefono_institucional: '318 12932 2342',
        telefono_movil: '23847239847',
        direccion_fisica: 'Street 43',
      },
    ],

    user_claims: [
      {
        md_id: '1',
        module: 'Usuarios',
        md_title: 'Usuarios',
        md_link: 'usuarios',
        md_icon: 'dashboard',
        ft_id: '1',
        feature: 'Usuarios01',
        ft_title: 'Usuarios-01',
        ft_link: 'ft-usuarios-01',
        ft_icon: 'scatter_plot',
        can_access: true,
        can_edit: true,
      }
      ,{
        md_id: '2',
        module: 'Formularios',
        md_title: 'Formularios',
        md_link: 'md-formulario',
        md_icon: 'dynamic_form', // md-formulario/ft-disenador
        ft_id: '1',
        feature: 'Diseñador', 
        ft_title: 'Diseñador',
        ft_link: 'ft-disenador',
        ft_icon: 'style',
        can_access: true,
        can_edit: true,
      },{
        md_id: '2',
        module: 'Formularios',
        md_title: 'Formularios',
        md_link: 'md-formulario',
        md_icon: 'dynamic_form', // md-formulario/ft-disenador
        ft_id: '1',
        feature: 'Administrar-Formulario', 
        ft_title: 'Administrar',
        ft_link: 'ft-administrar',
        ft_icon: 'fact_check',
        can_access: true,
        can_edit: true,
      }
      ,{
        md_id: '3',
        module: 'Seguimiento',
        md_title: 'Seguimiento',
        md_link: 'md-seguimiento',
        md_icon: 'view_quilt',
        ft_id: '1',
        feature: 'Seguimiento01', 
        ft_title: 'Seguimiento-01',
        ft_link: 'ft-seguimiento-01',
        ft_icon: 'style',
        can_access: true,
        can_edit: true,
      },
      {
        md_id: '5',
        module: 'Test',
        md_title: 'Test',
        md_link: 'md-test',
        md_icon: 'view_quilt',
        ft_id: '1',
        feature: 'test-01', 
        ft_title: 'test-01',
        ft_link: 'ft-test',
        ft_icon: 'style',
        can_access: true,
        can_edit: true,
      },
    ],
  },
};

//Object login  --  rslt = dt['user']['UserRoles']
//Role -- rslt['rolId']
//Modules -- mdls = rslt['rolId']['AccessRoles']['accessId']
// SubModules -- sbMdls = mdls['AccessComponents']

export const newDtTmp = {
  "user": {
      "id": 28,
      "name": "Wilson",
      "nickName": "Wilson.Lopez@neoris.com",
      "password": "12345678",
      "createDate": "2020-07-07T21:21:32.000+0000",
      "modify": "2020-10-06T00:00:00.000+0000",
      "status": true,
      "dateInit": "2020-07-07T00:00:00.000+0000",
      "dateFin": "2020-07-07T00:00:00.000+0000",
      "timeInit": 0,
      "timeFin": 0,
      "commentary": null,
      "defaultIcon": true,
      "photo": null,
      "UserRoles": [
          {
              "id": 12,
              "rolId": {
                  "id": 15,
                  "name": "Administrador",
                  "description": "Administrador",
                  "createDate": "2020-07-07T21:21:32.000+0000",
                  "modify": "2020-07-07T21:21:32.000+0000",
                  "status": true,
                  "commentary": null,
                  "AccessRoles": [
                      {
                          "id": 3,
                          "createDate": "2020-07-07T21:21:32.000+0000",
                          "modify": "2020-07-07T21:21:32.000+0000",
                          "status": true,
                          "commentary": null,
                          "accessId": {
                              "id": 21,
                              "name": "Configuración Generals",
                              "description": "Acceso a Configuración General",
                              "createDate": "2020-07-07T00:00:00.000+0000",
                              "modify": "2020-07-07T00:00:00.000+0000",
                              "status": true,
                              "commentary": null,
                              "AccessComponents": [
                                  {
                                      "id": 17,
                                      "componentsId": {
                                          "id": 49,
                                          "name": "ConfiguracionGeneral",
                                          "description": "Modulo Configuracion Generals",
                                          "createDate": "2020-07-07T21:19:22.000+0000",
                                          "modify": "2020-07-07T21:29:49.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "MODULO"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Todos"
                                  },
                                  {
                                      "id": 18,
                                      "componentsId": {
                                          "id": 54,
                                          "name": "Componentes",
                                          "description": "SubModulo Componentes",
                                          "createDate": "2020-07-07T21:19:22.000+0000",
                                          "modify": "2020-07-07T21:19:22.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "SUBMODULO"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Editar"
                                  },
                                  {
                                      "id": 19,
                                      "componentsId": {
                                          "id": 55,
                                          "name": "Permisos",
                                          "description": "SubModulo Permisos",
                                          "createDate": "2020-07-07T21:19:22.000+0000",
                                          "modify": "2020-07-07T21:19:22.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "SUBMODULO"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Listar"
                                  },
                                  {
                                      "id": 20,
                                      "componentsId": {
                                          "id": 56,
                                          "name": "Roles",
                                          "description": "SubModulo Roles",
                                          "createDate": "2020-07-07T21:19:22.000+0000",
                                          "modify": "2020-07-07T21:19:22.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "SUBMODULO"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Consultar"
                                  },
                                  {
                                      "id": 21,
                                      "componentsId": {
                                          "id": 57,
                                          "name": "Usuarios",
                                          "description": "SubModulo Usuarios",
                                          "createDate": "2020-07-07T21:19:22.000+0000",
                                          "modify": "2020-07-07T21:19:22.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "SUBMODULO"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Crear"
                                  },
                                  {
                                      "id": 22,
                                      "componentsId": {
                                          "id": 58,
                                          "name": "Listar Componentes",
                                          "description": "Pagina Listar Componentes",
                                          "createDate": "2020-07-07T21:21:32.000+0000",
                                          "modify": "2020-07-07T21:21:32.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "PAGINA"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Todos"
                                  },
                                  {
                                      "id": 23,
                                      "componentsId": {
                                          "id": 59,
                                          "name": "Crear Componentes",
                                          "description": "Pagina Crear Componentes",
                                          "createDate": "2020-07-07T21:21:32.000+0000",
                                          "modify": "2020-07-07T21:21:32.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "PAGINA"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Editar"
                                  },
                                  {
                                      "id": 24,
                                      "componentsId": {
                                          "id": 60,
                                          "name": "Editar Componentes",
                                          "description": "Pagina Editar Componentes",
                                          "createDate": "2020-07-07T21:21:32.000+0000",
                                          "modify": "2020-07-07T21:21:32.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "PAGINA"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Listar"
                                  },
                                  {
                                      "id": 25,
                                      "componentsId": {
                                          "id": 61,
                                          "name": "Listar Permisos",
                                          "description": "Pagina Listar Permisos",
                                          "createDate": "2020-07-07T21:21:32.000+0000",
                                          "modify": "2020-07-07T21:21:32.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "PAGINA"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Consultar"
                                  },
                                  {
                                      "id": 26,
                                      "componentsId": {
                                          "id": 62,
                                          "name": "Crear Permisos",
                                          "description": "Pagina Crear Permisos",
                                          "createDate": "2020-07-07T21:21:32.000+0000",
                                          "modify": "2020-07-07T21:21:32.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "PAGINA"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Crear"
                                  },
                                  {
                                      "id": 27,
                                      "componentsId": {
                                          "id": 63,
                                          "name": "Editar Permisos",
                                          "description": "Pagina Editar Permisos",
                                          "createDate": "2020-07-07T21:21:32.000+0000",
                                          "modify": "2020-07-07T21:21:32.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "PAGINA"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Crear"
                                  },
                                  {
                                      "id": 28,
                                      "componentsId": {
                                          "id": 64,
                                          "name": "Listar Roles",
                                          "description": "Pagina Listar Roles",
                                          "createDate": "2020-07-07T21:21:32.000+0000",
                                          "modify": "2020-07-07T21:21:32.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "PAGINA"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Crear"
                                  },
                                  {
                                      "id": 29,
                                      "componentsId": {
                                          "id": 65,
                                          "name": "Crear Roles",
                                          "description": "Pagina Crear Roles",
                                          "createDate": "2020-07-07T21:21:32.000+0000",
                                          "modify": "2020-07-07T21:21:32.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "PAGINA"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Crear"
                                  },
                                  {
                                      "id": 30,
                                      "componentsId": {
                                          "id": 66,
                                          "name": "Editar Roles",
                                          "description": "Pagina Editar Roles",
                                          "createDate": "2020-07-07T21:21:32.000+0000",
                                          "modify": "2020-07-07T21:21:32.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "PAGINA"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Crear"
                                  },
                                  {
                                      "id": 31,
                                      "componentsId": {
                                          "id": 67,
                                          "name": "Listar Usuarios",
                                          "description": "Pagina Listar Usuarios",
                                          "createDate": "2020-07-07T21:21:32.000+0000",
                                          "modify": "2020-07-07T21:21:32.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "PAGINA"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Crear"
                                  },
                                  {
                                      "id": 32,
                                      "componentsId": {
                                          "id": 68,
                                          "name": "Crear Usuarios",
                                          "description": "Pagina Crear Usuarios",
                                          "createDate": "2020-07-07T21:21:32.000+0000",
                                          "modify": "2020-07-07T21:21:32.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "PAGINA"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Crear"
                                  },
                                  {
                                      "id": 33,
                                      "componentsId": {
                                          "id": 69,
                                          "name": "Editar Usuarios",
                                          "description": "Pagina Editar Usuarios",
                                          "createDate": "2020-07-07T21:21:32.000+0000",
                                          "modify": "2020-07-07T21:21:32.000+0000",
                                          "status": true,
                                          "commentary": null,
                                          "url": null,
                                          "type": "PAGINA"
                                      },
                                      "createDate": "2020-07-07T21:21:32.000+0000",
                                      "modify": "2020-07-07T21:21:32.000+0000",
                                      "status": true,
                                      "commentary": null,
                                      "function": "Crear"
                                  }
                              ]
                          }
                      }
                  ]
              },
              "rolPrincipal": true,
              "createDate": "2020-07-07T21:21:32.000+0000",
              "modify": "2020-07-07T21:21:32.000+0000",
              "status": true,
              "commentary": null,
              "usersId": null
          }
      ]
  },
  "token": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzb2Z0dGVrSldUIiwic3ViIjoid2lsc29uLmxvcGV6QG5lb3Jpcy5jb20iLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwiaWF0IjoxNjAzODM3MTMzLCJleHAiOjE2MDM4Mzc3MzN9.w4Iify1m2Wi2qck-t3xHayaxx3sYFr6CiasFBMuYqEGK1_qXcgMUlkgHhElNcJCshfW73kayr50ucV-f-ByhFA",
  "messages": "OK",
  "codError": "200"
};

// {
//   "objModel": 
// };