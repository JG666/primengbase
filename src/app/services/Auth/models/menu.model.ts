export class UsuarioMenuModelo
{
    Id: number;
    RolId: number;
    NombreRol: string;
    NoDocumento: string;
    TipoDocumento: string;
    Nombres: string;
    Apellidos: string;
    Email: string;
    user_claims: string;
}