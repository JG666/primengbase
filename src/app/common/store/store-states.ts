import { userPermissionsclaims } from '../../auth/models/org.model';
import { UserLoginModel } from '../../auth/models/auth.model';
import { UserPermissionsModel } from '../../auth/models/org.model';
import { CountryMdl } from 'src/app/core/services/models/location.model';
import { InstitutionMdl } from 'src/app/core/services/models/institution.model';


// Versioning state
export enum VersionActions {
  GetVersion = 'GET_VERSION',
  SetVersion = 'SET_VERSION',
}
export interface VersionStoreState {
  currentVersion: String;
  upToDate: Boolean;
  date: String;
}


// Right Menu Store
export enum RightMenuActions {
  InitialRightMenu = 'INIT_RIGHT_MENU',
  SetRightMenuOp = 'SET_RIGHT_MENU_OPTION',
}

export interface RightMenuStates {
  notifications : any[];
  wsStatusComm : boolean;
}

  // -- Menu 
  export enum MenuActions {
    InitialState = 'INIT_MENU_STORE',
    GetOp = 'GET_OPTIONS',
    SetOp = 'SET_OPTIONS',
    SetMenu = 'SET_MENU',
  }

  export interface MenuStates {
    displayModules : boolean;
    menuMode : String;
    lightMenu : String;
    overlayMenuActive : boolean;
    staticMenuMobileActive : String;
    staticMenuDesktopInactive : String;
    topbarColor : String;
    resetMenu : boolean;
    generalMenu : {};
    menuItems : any[];
    rightMenuClick: boolean;
    onMenuButtonClick : any[];
  }

  // Store User
export enum AuthStoreActions {
  UserLogin = 'USER_SINGIN',
  Token = 'SET_TOKEN',
  ResfeshToken = 'SET_REFRESH_TOKEN',
  InitAuthStore = 'INIT_AUTH_STORE',
  AuthStatus = 'AUTH_STATUS',
}

export interface AuthStoreState {
  authStatus: boolean;
  userLogin: UserLoginModel;
  token: string;
  refreshToken: string;
}

// user Management
export enum UsrPermissionsStoreActions {
  UserPermisions = 'USER_PERMISSIONS',
  InitPermissionsStore = 'INIT_PERMISSIONS'
}

export interface UsrPermissionsStoreState {
  userPermisions: UserPermissionsModel[];
}

// ModulesFeatures
export enum ModulesFeaturesStoreActions {
  ModulesFeatures = 'MODULES_FEATURES',
  InitModulesFeaturesStore = 'INIT_MODULES_FEATURES'
}

export interface ModulesFeaturesStoreState {
  modulesFeatures: userPermissionsclaims[];
}



  // -- Locations 
  export enum LocationsActions {
    InitialState = 'INIT_LOCATION_STORE',
    SetOp = 'SET_OPTIONS',
    SetLocations = 'SET_MENU',
  }
  export interface LocationsStates {
    countries : CountryMdl[];
  }

  // -- Institution 
  export enum InstitutionActions {
    InitialState = 'INIT_LOCATION_STORE',
    SetOp = 'SET_OPTIONS',
    SetInstitution = 'SET_MENU',
  }
  export interface InstitutionStates {
    institutions : InstitutionMdl[];
  }


