import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppMenuComponent,AppSubMenuComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RightMenuComponent } from './right-menu/right-menu.component';
import { SubMenuComponent } from './sidebar/sub/sub-menu.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BreadCrumbComponent } from './breadcrumb/breadcrumb.component';

@NgModule({
  imports:[
    CommonModule,
    SharedModule
],
declarations:[
    AppMenuComponent,
    HeaderComponent,
    BreadCrumbComponent,
    FooterComponent,
    RightMenuComponent,
    SubMenuComponent,
    AppSubMenuComponent,
    
],
exports: [
    AppMenuComponent,
    HeaderComponent,
    FooterComponent,
    BreadCrumbComponent,
    RightMenuComponent,
    SubMenuComponent,
    AppSubMenuComponent,    
],   
schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class MenuAppModule { }
