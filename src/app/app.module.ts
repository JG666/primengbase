import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, /*HttpClient,*/ HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AppRoutesModule } from './app.routes';

import { AppComponent } from './app.component';
import { PagesModule } from './core/pages.module';

import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { LocalStorageService } from './services/Auth/local-storage.service';
import { AuthHttpInterceptor } from './services/http/auth-httpInterceptor.service';
import { ErrorsHandler } from './services/http/error-handler.service';
import { MessageService } from 'primeng/api';
import { LoggingService } from './services/http/logging.service';
import { EncodeHttpService } from './services/http/Encode-Http.service';

import { OverlayLoaderInterceptor } from './services/http/overlay-interceptor.service';
import { OverlayLoaderComponent } from './common/overlay/overlay-loader.component';
import { OverlayService } from './services/overlay.service';
import { DropResquestService } from './services/http/drop-request.service';
import { AuthModule } from './auth/auth.module';
import { SharedModule } from './shared/shared.module';
import { AuthService } from './services/Auth/auth.service';
import { MomentDateService } from './services/moment.service';

@NgModule({
    imports: [    
        BrowserModule,
        AppRoutesModule,
        AuthModule,
        PagesModule,
        HttpClientModule,
        ReactiveFormsModule,
        FormsModule,
        BrowserAnimationsModule,
        LoadingBarHttpClientModule,
        LoadingBarModule,
        SharedModule,
        
    ],
    declarations: [
        AppComponent,
        OverlayLoaderComponent, 
    ],
    exports: [SharedModule],
    providers: [
        MomentDateService,
        OverlayService,
        DropResquestService,
        LocalStorageService,
        LoggingService,
        MessageService,
        AuthService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthHttpInterceptor,
          multi: true
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: EncodeHttpService,
          multi: true
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: OverlayLoaderInterceptor,
          multi: true
        },
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        { provide: ErrorHandler, useClass: ErrorsHandler },

    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
