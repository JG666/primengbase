import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ModulosComponent } from './modulos.component';
import { SubModuloComponent } from './submodulo/submodulo.component';

const routes: Routes = [
    {path: '', component: ModulosComponent },
    { path: 'submodulo', component : SubModuloComponent},
  ];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ ModulosComponent, SubModuloComponent]
})
export class ModulosModule { }
