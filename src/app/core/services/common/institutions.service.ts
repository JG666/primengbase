import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ObservableStore } from '@codewithdan/observable-store';
import { throwError, Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { InstitutionActions, InstitutionStates } from 'src/app/common/store/store-states';
import { environment } from 'src/environments/environment';
import { InstitutionMdl } from '../models/institution.model';
const headers = new HttpHeaders().set('Content-Type', 'application/json');

@Injectable({providedIn: 'root'})
export class InstitutionService extends ObservableStore<InstitutionStates>{

    constructor(private http: HttpClient) {
        super({ stateSliceSelector: state => {return {
            institutions: (state !== null ) ? state.institutions : [],
        }}});
     }

    getInstitutions(): Observable<InstitutionMdl[]> {
        return this.http
        .get<any>(`${environment.base_url}/excelsis-api/institucions/get`,{headers})
        .pipe(map(dt => dt.object),tap(dt => this.setStoreOp('institutions',dt)),catchError(this.handleError));
    }


    setStoreOp(op, dt, wh?): any {
        switch (op) {
            case 'institutions':
                this.setState({ institutions: dt }, InstitutionActions.SetOp);
        }
    }

    getStoreOp(op): any {
        switch (op) {
            case 'institutions':
                return this.getState().institutions;
        }
    }

    private handleError(err) {
        console.log('handleError ***',err);
        let errorMessage: string;
        if (err.error instanceof ErrorEvent)
            errorMessage = `An error occurred: ${JSON.stringify((err.error && err.error.Message)|| err.message)}`;
         else
            errorMessage = `Backend returned error: ${JSON.stringify((err.error && err.error.Message) || err.message)}`;
        return throwError(errorMessage);
    }

}