import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ObservableStore } from '@codewithdan/observable-store';
import { throwError, Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { LocationsActions, LocationsStates } from 'src/app/common/store/store-states';
import { environment } from 'src/environments/environment';
import { CountryMdl } from '../models/location.model';
const headers = new HttpHeaders().set("Content-Type", "application/json",);

@Injectable({providedIn: 'root'})
export class LocationService  extends ObservableStore<LocationsStates> {

    constructor(private http: HttpClient) { 
        super({ stateSliceSelector: state => {return {
            countries: (state !== null ) ? state.countries : [],
        }}});
    }

    getCountries(): Observable<CountryMdl[]> {
        return this.http
        .get<any>(`${environment.base_url}/excelsis-api/countries/get`,{headers})
        .pipe(map(dt => dt.object),tap(dt => this.setStoreOp('countries',dt)),catchError(this.handleError));
    }


    setStoreOp(op, dt, wh?): any {
        switch (op) {
            case 'countries':
                this.setState({ countries: dt }, LocationsActions.SetOp);
        }
    }

    getStoreOp(op): any {
        switch (op) {
            case 'countries':
                return this.getState().countries;
        }
    }

    private handleError(err) {
        console.log('handleError ***',err);
        let errorMessage: string;
        if (err.error instanceof ErrorEvent) 
            errorMessage = `An error occurred: ${JSON.stringify((err.error && err.error.Message)|| err.message)}`;
         else 
            errorMessage = `Backend returned error: ${JSON.stringify((err.error && err.error.Message) || err.message)}`;
        return throwError(errorMessage);
    }

}