export class CountryMdl {
    id: number;
    name: string;
    description: string;
    formalDescription: string;
    createDate: string;
    modify: string;
    status: boolean;
    codeDane: string;
    codePrefijo: string;
    codePostal: string;
    codeCountry: string;
    commentary: string;
}
