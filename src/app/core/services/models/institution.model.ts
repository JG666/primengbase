import { CountryMdl } from "./location.model";


export class InstitutionMdl{
    id:number;
    name:string;
    description:string;
    logo:string;
    code:string;
    phone:string;
    address:string;
    typeInstitution:string;
    createDate:string;
    modify:string;
    status:boolean;
    commentary:string;
    countries:CountryMdl;
}

