import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { VisionMdl, VisionXMdl } from '../../models/vision.models';
import { LocationService } from 'src/app/core/services/common/locations.service';
import { InstitutionService } from 'src/app/core/services/common/institutions.service';
import { MessageService } from 'primeng/api';
import { MisionService } from '../../services/mision.service';
import { MomentDateService } from 'src/app/services/moment.service';
import { SubSink } from 'subsink';

export class MisionData {
  mode: string;
  object
}
@Component({
  selector: 'app-mision-mdl',
  templateUrl: './mision-mdl.component.html',
  styleUrls: ['./mision-mdl.component.css']
})
export class MisionMdlComponent implements OnInit  {

  private subSink = new SubSink();
  localDT: MisionData;
  form: FormGroup;
  misionObj: VisionMdl;
  institutionSelected: string;
  institutionQryRslt: any[];
  coutriesQryRslt: any[];
  submitted = false;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
              private visionSvc:MisionService, private fb: FormBuilder,
                private locationService:LocationService, private institutionService:InstitutionService,
                  private messageService: MessageService, private momentDateService: MomentDateService) {
    this.localDT = this.config.data;
    console.log('data recieved at modal ***',this.localDT);
  }

  ngOnInit() {
    this.form = this.fb.group({
      name : ['', Validators.required],
      institution : ['', Validators.required],
      country : ['', Validators.required],
      status : [true, Validators.required],
    });

    if(this.localDT.mode === 'edit') {
      const dt = {...this.localDT.object};
      const x = {
        id: dt.id,
        name: dt.name,
        country: {name : dt.institutionId.countries.name, code: dt.institutionId.countries.name}, 
        institution: {name : dt.institutionId.name, code: dt.institutionId.name},
        status: dt.status,
      } ;
      this.form.patchValue({...x});
    }
  }

search(dt, op) {
  switch (op) {
    case 'country':
      this.coutriesQryRslt = this.locationService.getStoreOp('countries').filter(d => d.name.toLowerCase().includes(dt.query.toLowerCase()));
    break;
    case 'institution':
      this.institutionQryRslt = this.institutionService.getStoreOp('institutions').filter(d => d.name.toLowerCase().includes(dt.query.toLowerCase()));
    break;
  }
}

handleDropdown(event) {
    //event.query = current value in input field
}

hideDialog() {
  this.submitted = false;
  this.ref.close({mode:null, object: null } as MisionData);
}

saveObject() {
    this.submitted = true;
    if (this.form.valid) {
      const objValues = this.form.value;
        if (this.localDT.mode === 'edit') {
          const dtToEdit = {...this.localDT.object};
          const xDt = {
            id: dtToEdit.id, 
            name:  objValues.description,
            country:  objValues.country.id,
            status:   objValues.status,
            institutionId: {id: objValues.institution.id},
            createDate: objValues.createDate,
            modify: this.momentDateService.getUtcNow()
          } as VisionXMdl;
          this.onCreateVision(xDt);
        }
        else {
          const xDt = {
            id: objValues.id, 
            name:  objValues.name,
            country: {id: objValues.country.id} ,
            status: objValues.status,
            institutionId: {id: objValues.institution.id},
            createDate: this.momentDateService.getUtcNow(),
            modify: ''            
          } as VisionXMdl;
          this.onCreateVision(xDt);
        }
    }
}

//#region handle CRUD Vision
  
onCreateVision(rslt: VisionXMdl) {   
      this.subSink.sink = this.visionSvc.proxy(rslt, this.localDT.mode).subscribe(rs => {
        this.resetModalObj({});
        this.messageService.add({key:'mdl-vsn',severity:'success', summary: 'Registro actualizado!.'});
      }, 
      (err) => {
        this.messageService.add({key:'mdl-vsn',severity:'error', summary: 'No se realizo la operación!.', detail: err});
      });     
}

resetModalObj(dt:any) {
  this.form.reset();
  this.submitted = false;
  setTimeout(() => {
    this.ref.close({mode: this.localDT.mode, object: dt } as MisionData);
  }, 300);
}

//#endregion

ngOnDestroy(): void {
  this.subSink.unsubscribe();
}

}

