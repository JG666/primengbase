
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/services/Auth/auth-guard.service';
import { MisionComponent } from './mision.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MomentDateService } from 'src/app/services/moment.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { MisionMdlComponent } from './modal/mision-mdl.component';
import { DialogService } from 'primeng/dynamicdialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocationService } from '../../services/common/locations.service';
import { InstitutionService } from '../../services/common/institutions.service';
import { MisionService } from '../services/mision.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '',canActivate: [AuthGuardService], component: MisionComponent }]),
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [MisionComponent,MisionMdlComponent],
  providers: [MisionService, MomentDateService, MessageService, ConfirmationService,DialogService,
                LocationService,InstitutionService],
})
export class MisionModule { }
