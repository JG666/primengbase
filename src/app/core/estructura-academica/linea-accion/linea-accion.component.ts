import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Table } from 'primeng/table';
import { take, catchError } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { InstitutionService } from '../../services/common/institutions.service';
import { LocationService } from '../../services/common/locations.service';
import { InstitutionMdl } from '../../services/models/institution.model';
import { CountryMdl } from '../../services/models/location.model';
import { LineaAccionService } from '../services/linea-accion.service';
import { LineaAccionMdlComponent } from './modal/linea-accion-mdl.component';

@Component({
  selector: 'app-linea-accion',
  templateUrl: './linea-accion.component.html',
  styleUrls: ['./linea-accion.component.css']
})
export class LineaAccionComponent implements OnInit, OnDestroy  {
  
  title = 'Línea Acción'
  private subSink = new SubSink();
  academicData:any[];
  @ViewChild('tblMision') table: Table;
  selectItemVsn: any;
  coutrySrc: CountryMdl[];
  instituteSrc: InstitutionMdl[];
  ref: DynamicDialogRef;

  generalObjectVision = {
    paisSelected:'',
    institutionSelected:'',
    filteredCountries: [] as CountryMdl[],
    filteredInstitutions: [] as InstitutionMdl[],
  }

  constructor(private cmpntLclSvc:LineaAccionService, public dialogService: DialogService, 
                private messageService: MessageService, private confirmationService: ConfirmationService,
                  private locationService:LocationService, private institutionService:InstitutionService) {}

  ngOnInit() {
    this.subSink.sink =  this.cmpntLclSvc.get().pipe(take(1)).subscribe(rs => {
      const {object} = rs
      this.academicData = object.map(o => { return {...o, createDate: (o['createDate']).split('T')[0], modify: (o['modify']).split('T')[0] }});
    });

    this.subSink.sink =  this.locationService.getCountries().pipe(take(1))
      .subscribe((rs:CountryMdl[]) => {
        this.coutrySrc = rs;
    });

    this.subSink.sink =  this.institutionService.getInstitutions().pipe(take(1))
      .subscribe((rs:InstitutionMdl[]) => {
        this.instituteSrc = rs;
    });

  }

  search(dt, op) {
    console.log('query search',dt);
    switch (op) {
      case 'country':
        this.generalObjectVision.filteredCountries = this.coutrySrc.filter(d => d.name.toLowerCase().includes(dt.query.toLowerCase()));
      break;
      case 'institution':
        this.generalObjectVision.filteredInstitutions = this.instituteSrc.filter(d => d.name.toLowerCase().includes(dt.query.toLowerCase()));
      break;
    }
  }

  ddSelectGeneric(e,fn) {
    console.log('ddSelectGeneric***',e,e.trim().length);
    this[fn](e);
  }

  deleteSelectItemVsn() {
      console.log('deleteSelectItemVsn***',this.selectItemVsn);
      this.confirmationService.confirm({
          message: 'Esta seguro de eliminar este registro?',
          header: 'Confirm',
          icon: 'pi pi-exclamation-triangle',
          accept: () => {
            this.onDeletingObj(this.selectItemVsn);
          },
          reject : () => {
            this.cleaningView();
          }
      });
  } 

  onDeletingObj(data) { //deleteVision
    this.academicData = this.academicData.filter(i => data.id !== i.id);
    this.selectItemVsn = null;
    this.cmpntLclSvc.delete(data).subscribe(rs => {
      console.log('result onDeletingObj vision ***', rs);
      this.messageService.add({severity:'success', summary: 'Successful', detail: 'Registro Eliminado', life: 3000});
    }, (err) => {
        console.log('result onDeletingObj vision ***', err);
        this.messageService.add({severity:'error', summary: 'Data deleting', detail: 'item....'});
    });
  }

  openNew(mode) {
    if(mode === 'new') this.cleaningView();
    this.ref = this.dialogService.open(LineaAccionMdlComponent, {
        data: { mode, object: this.selectItemVsn  },
        header: `${mode === 'edit' ? 'Editar' : 'Crear'} ${this.title}`,
        width: '60%',
        contentStyle: {'max-height': '500px', 'overflow': 'auto'},
        baseZIndex: 10000
    });

    this.ref.onClose.subscribe(rslt => {
        console.log('Data recieved from modal ***',rslt);
        this.cleaningView();
      });
  }

  cleaningView() {
    this.selectItemVsn = null;
  }
    

  //#region  generic functions 

    onCountrySelectChange(value) {
      if (value && value.trim().length) {
          this.table.filter(value, 'institutionId.name', 'equals');
      }
    }

    onInstitutionSelectChange(value) {
      if (value && value.trim().length) {
          this.table.filter(value, 'name', 'contains');
      }
    }
    
    hndlClearSelect(op) {
      switch (op) {
        case 'paisSelected':
          this.generalObjectVision.paisSelected = '';
          this.table.filter('', 'institutionId.name', 'equals');
        break;
        case 'institutionSelected':
          this.generalObjectVision.institutionSelected = '';
          this.table.filter('', 'name', 'equals');
        break;
      
      }
    }

 //#endregion

  ngOnDestroy(): void {
    if (this.ref) 
      this.ref.close();
    this.subSink.unsubscribe();
  }
}

