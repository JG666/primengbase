import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { VisionMdl, VisionXMdl } from '../../models/vision.models';
import { LocationService } from 'src/app/core/services/common/locations.service';
import { InstitutionService } from 'src/app/core/services/common/institutions.service';
import { MessageService } from 'primeng/api';
import { LineaAccionService } from '../../services/linea-accion.service';

export class MisionData {
  mode: string;
  object
}
@Component({
  selector: 'app-linea-accion-mdl',
  templateUrl: './linea-accion-mdl.component.html',
  styleUrls: ['./linea-accion-mdl.component.css']
})
export class LineaAccionMdlComponent implements OnInit  {

  localDT: MisionData;
  form: FormGroup;
  visionObj: VisionMdl;
  institutionSelected: string;
  institutionQryRslt: any[];
  coutriesQryRslt: any[];
  submitted = false;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
              private cmpntLclSvc:LineaAccionService, private fb: FormBuilder,
                private locationService:LocationService, private institutionService:InstitutionService,
                  private messageService: MessageService,) {
    this.localDT = this.config.data;
    console.log('data recieved from parent ***', this.localDT);
  }

    ngOnInit() {
      this.form = this.fb.group({
        name : ['', Validators.required],
        description : ['', Validators.required],
        institution : ['', Validators.required],
        country : ['', Validators.required],
        status : [false, Validators.required],
      });

      if(this.localDT.mode === 'edit') {
        const dt = {...this.localDT.object};
        const x = {
          id: dt.id,
          description: dt.name,
          country: {name : dt.institutionId.countries.name, code: dt.institutionId.countries.name}, 
          institution: {name : dt.institutionId.name, code: dt.institutionId.name},
          status: dt.status,
        } ;
        this.form.patchValue({...x});
      }
    }

  search(dt, op) {
    switch (op) {
      case 'country':
        this.coutriesQryRslt = this.locationService.getStoreOp('countries').filter(d => d.name.toLowerCase().includes(dt.query.toLowerCase()));
      break;
      case 'institution':
        this.institutionQryRslt = this.institutionService.getStoreOp('institutions').filter(d => d.name.toLowerCase().includes(dt.query.toLowerCase()));
      break;
    }
  }

  handleDropdown(event) {
      //event.query = current value in input field
  }

  hideDialog() {
    this.submitted = false;
    this.ref.close({mode:null, object: null } as MisionData);
  }
  
  saveObject() {
      this.submitted = true;
      if (this.form.valid) {
        const objValues = this.form.value;
          if (this.localDT.mode === 'edit') {
            const dtToEdit = {...this.localDT.object};
            const xDt = {
              id: dtToEdit.id, description:  objValues.description,
              country:  objValues.country.id,
              institution:   objValues.institution.id,
              status:   objValues.status,
            };
            this.onCreateVision(xDt);
          }
          else {
            const xDt = {
              id: objValues.id, description:  objValues.description,
              country:  objValues.country.id,
              institution:   objValues.institution.id,
              status:   objValues.status,
            };
            this.onCreateVision(xDt);
          }
      }
  }


  //#region handle CRUD Vision
    
  onCreateVision(rslt) {    
    this.cmpntLclSvc.create(rslt).subscribe(rs => {
      console.log('result create vision ***', rs);
      this.resetModalObj({});
      this.messageService.add({severity:'success', summary: 'Data saving', detail: 'item....'});
    }, 
    (err) => {
      console.log('result create vision ***', err);
      this.messageService.add({severity:'error', summary: 'Data saving', detail: 'item....'});
    });
  }

  resetModalObj(dt:any) {
    this.form.reset();
    this.submitted = false;
    setTimeout(() => {
      this.ref.close({mode: this.localDT.mode, object: dt } as MisionData);
    }, 200);
  }

//#endregion

}

