
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/services/Auth/auth-guard.service';
import { LineaAccionComponent } from './linea-accion.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MomentDateService } from 'src/app/services/moment.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { LineaAccionMdlComponent } from './modal/linea-accion-mdl.component';
import { DialogService } from 'primeng/dynamicdialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocationService } from '../../services/common/locations.service';
import { InstitutionService } from '../../services/common/institutions.service';
import { LineaAccionService } from '../services/linea-accion.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '',canActivate: [AuthGuardService], component: LineaAccionComponent }]),
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [LineaAccionComponent,LineaAccionMdlComponent],
  providers: [LineaAccionService, MomentDateService, MessageService, ConfirmationService,DialogService,
                LocationService,InstitutionService],
})
export class LineaAccionModule { }
