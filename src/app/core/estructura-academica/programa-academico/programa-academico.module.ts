
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/services/Auth/auth-guard.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { MomentDateService } from 'src/app/services/moment.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { ProgramaAcademicoMdlComponent } from './modal/programa-academico-mdl.component';
import { DialogService } from 'primeng/dynamicdialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocationService } from '../../services/common/locations.service';
import { InstitutionService } from '../../services/common/institutions.service';
import { ProgramaAcademicoComponent } from './programa-academico.component';
import { ProgramaAcademicoService } from '../services/programa-academico.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '',canActivate: [AuthGuardService], component: ProgramaAcademicoComponent }]),
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [ProgramaAcademicoComponent,ProgramaAcademicoMdlComponent],
  providers: [ProgramaAcademicoService, MomentDateService, MessageService, ConfirmationService,DialogService,
                LocationService,InstitutionService],
})
export class ProgramaAcademicoModule { }
