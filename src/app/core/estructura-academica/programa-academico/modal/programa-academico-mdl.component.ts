import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { VisionMdl, VisionXMdl } from '../../models/vision.models';
import { LocationService } from 'src/app/core/services/common/locations.service';
import { InstitutionService } from 'src/app/core/services/common/institutions.service';
import { MessageService } from 'primeng/api';
import { ConfigApp } from 'src/app/core/services/models/config-app.model';
import { ProgramaAcademicoService } from '../../services/programa-academico.service';

export class VisionData {
  mode: string;
  object
}
@Component({
  selector: 'app-programa-academico-mdl',
  templateUrl: './programa-academico-mdl.component.html',
  styleUrls: ['./programa-academico-mdl.component.css']
})
export class ProgramaAcademicoMdlComponent implements OnInit  {

  localDT: VisionData;
  form: FormGroup;
  visionObj: VisionMdl;
  institutionSelected: string;
  institutionQryRslt: any[];
  coutriesQryRslt: any[];
  submitted = false;
  es: any;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
              private cmpntLclSvc:ProgramaAcademicoService, private fb: FormBuilder,
                private locationService:LocationService, private institutionService:InstitutionService,
                  private messageService: MessageService,) {
    this.localDT = this.config.data;
    console.log('data recieved from parent ***', this.localDT);
  }

    ngOnInit() {
      this.form = this.fb.group({
        code : ['', Validators.required],
        description : ['', Validators.required],
        institution : ['', Validators.required],
        country : ['', Validators.required],
        status : [false, Validators.required],
      });

      this.es = ConfigApp['calendar-spanish'];

      if(this.localDT.mode === 'edit') {
        const dt = {...this.localDT.object};
        const x = {
          id: dt.id,
          description: dt.name,
          country: {name : dt.institutionId.countries.name, code: dt.institutionId.countries.name}, 
          institution: {name : dt.institutionId.name, code: dt.institutionId.name},
          status: dt.status,
        } ;
        this.form.patchValue({...x});
      }
    }


    onDateSelect(e,op) {
      // switch (op) {
      //     case 'init':
      //         this.fechas.fechaInicial = e;
      //         this.table.filter(this.utils.getUtcFormated(this.fechas.fechaInicial,'YYYY-MM-DD'), 'fechaCrea', 'gte');
      //     break;
      //         case 'end':
      //             if(!!this.fechas.fechaInicial) {
      //             this.fechas.fechaFinal = e;
      //             this.table.filter(this.utils.getUtcFormated(e,'YYYY-MM-DD'), 'fechaCrea', 'lte');
      //             console.log(this.fechas )
      //         }
      //     break;
      // }
  }

  search(dt, op) {
    switch (op) {
      case 'country':
        this.coutriesQryRslt = this.locationService.getStoreOp('countries').filter(d => d.name.toLowerCase().includes(dt.query.toLowerCase()));
      break;
      case 'institution':
        this.institutionQryRslt = this.institutionService.getStoreOp('institutions').filter(d => d.name.toLowerCase().includes(dt.query.toLowerCase()));
      break;
    }
  }

  handleDropdown(event) {
      //event.query = current value in input field
  }

  hideDialog() {
    this.submitted = false;
    this.ref.close({mode:null, object: null } as VisionData);
  }
  
  saveObject() {
      this.submitted = true;
      if (this.form.valid) {
        const objValues = this.form.value;
          if (this.localDT.mode === 'edit') {
            const dtToEdit = {...this.localDT.object};
            const xDt = {
              id: dtToEdit.id, description:  objValues.description,
              country:  objValues.country.id,
              institution:   objValues.institution.id,
              status:   objValues.status,
            } ;
            this.onCreateVision(xDt);
          }
          else {
            const xDt = {
              id: objValues.id, description:  objValues.description,
              country:  objValues.country.id,
              institution:   objValues.institution.id,
              status:   objValues.status,
            } ;
            this.onCreateVision(xDt);
          }
      }
  }


  //#region handle CRUD Vision
    
  onCreateVision(rslt) {    
    this.cmpntLclSvc.create(rslt).subscribe(rs => {
      console.log('result create vision ***', rs);
      this.resetModalObj({});
      this.messageService.add({severity:'success', summary: 'Data saving', detail: 'item....'});
    }, 
    (err) => {
      console.log('result create vision ***', err);
      this.messageService.add({severity:'error', summary: 'Data saving', detail: 'item....'});
    });
  }

  resetModalObj(dt:any) {
    this.form.reset();
    this.submitted = false;
    setTimeout(() => {
      this.ref.close({mode: this.localDT.mode, object: dt } as VisionData);
    }, 200);
  }

//#endregion

}

