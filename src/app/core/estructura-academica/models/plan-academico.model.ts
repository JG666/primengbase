export class PlanAcademicoMdl {
    id:number;
    description: string;
    country: string;
    institution: string;
    grade: string;
    academicProgram: string;
    academicPlan: string;
    name: string;
    createDate: string;
    modify: string;
    dateInit: string;
    dateFin: string;
    status: boolean;
}