import { InstitutionMdl } from "../../services/models/institution.model";

export class VisionMdl {
    id:number;
    description: string;
    country: string;
    institution: string;
}
export class VisionXMdl {
    id:number;
    name: string;
    commentary?: string;
    country: any;
    institutionId: any;
    createDate: string;
    modify: string;
}
export class VisionSrcMdl {
    id:number;
    commentary: string;
    createDate: string;
    modify: string;
    name: string;
    status: boolean;
    institutionId: InstitutionMdl;
}

  export class VisionData {
    mode: string;
    object
  }

  