import { InstitutionMdl } from "../../services/models/institution.model";

export class MisionMdl {
    id:number;
    description: string;
    country: string;
    institution: string;
}
export class MisionXMdl {
    id:number;
    name: string;
    commentary?: string;
    country: any;
    institutionId: any;
    createDate: string;
    modify: string;
}
export class MisionSrcMdl {
    id:number;
    commentary: string;
    createDate: string;
    modify: string;
    name: string;
    status: boolean;
    institutionId: InstitutionMdl;
}

  export class MisionData {
    mode: string;
    object
  }

  