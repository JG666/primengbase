import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Table } from 'primeng/table';
import { take, catchError } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { InstitutionService } from '../../services/common/institutions.service';
import { LocationService } from '../../services/common/locations.service';
import { InstitutionMdl } from '../../services/models/institution.model';
import { CountryMdl } from '../../services/models/location.model';
import { VisionData, VisionSrcMdl } from '../models/vision.models';
import { VisionService } from '../services/vision.service';
import { VisionMdlComponent } from './modal/vision-mdl.component';

@Component({
  selector: 'app-vision',
  templateUrl: './vision.component.html',
  styleUrls: ['./vision.component.css']
})
export class VisionComponent implements OnInit, OnDestroy  {
 
  private subSink = new SubSink();
  dataSrc:VisionSrcMdl[];
  @ViewChild('tblVision') table: Table;
  selectItemVsn: any;
  coutrySrc: CountryMdl[];
  instituteSrc: InstitutionMdl[];
  ref: DynamicDialogRef;

  generalObjectVision = {
    paisSelected:'',
    institutionSelected:'',
    filteredCountries: [] as CountryMdl[],
    filteredInstitutions: [] as InstitutionMdl[],
  }

  constructor(private visionSvc:VisionService, public dialogService: DialogService, 
                private messageService: MessageService, private confirmationService: ConfirmationService,
                  private locationService:LocationService, private institutionService:InstitutionService) {}

  ngOnInit() {
    this.fetchInitialDataSrc();

    this.subSink.sink =  this.locationService.getCountries().pipe(take(1))
      .subscribe((rs:CountryMdl[]) => {
        this.coutrySrc = rs;
    });

    this.subSink.sink =  this.institutionService.getInstitutions().pipe(take(1))
      .subscribe((rs:InstitutionMdl[]) => {
        this.instituteSrc = rs;
    });

  }

  fetchInitialDataSrc() {
    this.subSink.sink =  this.visionSvc.get().subscribe(rs => {
      const {object} = rs
      console.log('data recieved from servidor ***',object);
      this.dataSrc = object.map(o => { return {...o, createDate: (o['createDate'] ).split('T')[0] || '', modify: (o['modify']).split('T')[0] || '' }});
    });
  }  

  search(dt, op) {
    console.log('query search',dt);
    switch (op) {
      case 'country':
        this.generalObjectVision.filteredCountries = this.coutrySrc.filter(d => d.name.toLowerCase().includes(dt.query.toLowerCase()));
      break;
      case 'institution':
        this.generalObjectVision.filteredInstitutions = this.instituteSrc.filter(d => d.name.toLowerCase().includes(dt.query.toLowerCase()));
      break;
    }
  }

  ddSelectGeneric(e,fn) {
    console.log('ddSelectGeneric***',e,e.trim().length);
    this[fn](e);
  }

  deleteSelectItemVsn() {
      console.log('deleteSelectItemVsn***',this.selectItemVsn);
      this.confirmationService.confirm({
          message: 'Esta seguro de eliminar este registro?',
          header: 'Confirm',
          icon: 'pi pi-exclamation-triangle',
          accept: () => {
            this.onDeletingObj(this.selectItemVsn);
          },
          reject : () => {
            this.cleaningView();
          }
      });
  } 

  onDeletingObj(data) {
    // this.dataSrc = this.dataSrc.filter(i => data.id !== i.id);
    this.selectItemVsn = null;
    this.visionSvc.delete(data).subscribe(rs => {
      this.fetchInitialDataSrc();
      this.messageService.add({key:'mdl-mn-vsn', severity:'success', summary: 'Registro actualizado!.',  life: 3000});
    }, (err) => {
        this.messageService.add({key:'mdl-mn-vsn', severity:'error', summary: 'No se realizo la operación!.', detail: err});
    });
  }

  openNew(mode) {
    if(mode === 'new') this.cleaningView();
    this.ref = this.dialogService.open(VisionMdlComponent, {
        data: { mode, object: this.selectItemVsn  } as VisionData,
        header: `${mode === 'edit' ? 'Editar' : 'Crear'} Visión`,
        width: '60%',
        contentStyle: {'max-height': '500px', 'overflow': 'auto'},
        baseZIndex: 10000
    });

    this.ref.onClose.subscribe((rslt:VisionData) => {
        console.log('Data recieved from modal ***',rslt);
        if(rslt.mode === 'new' || rslt.mode === 'edit') {
          this.fetchInitialDataSrc();
        }
        this.cleaningView();
      });
  }

  cleaningView() {
    this.selectItemVsn = null;
  }
    

  //#region  generic functions 

    onCountrySelectChange(value) {
      if (value && value.trim().length) {
          this.table.filter(value, 'institutionId.name', 'equals');
      }
    }

    onInstitutionSelectChange(value) {
      if (value && value.trim().length) {
          this.table.filter(value, 'name', 'contains');
      }
    }
    
    hndlClearSelect(op) {
      switch (op) {
        case 'paisSelected':
          this.generalObjectVision.paisSelected = '';
          this.table.filter('', 'institutionId.name', 'equals');
        break;
        case 'institutionSelected':
          this.generalObjectVision.institutionSelected = '';
          this.table.filter('', 'name', 'equals');
        break;
      
      }
    }

 //#endregion

  ngOnDestroy(): void {
    if (this.ref) 
      this.ref.close();
    this.subSink.unsubscribe();
  }
}

