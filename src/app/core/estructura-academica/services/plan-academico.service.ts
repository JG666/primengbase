import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
const headers = new HttpHeaders().set("Content-Type", "application/json",);

@Injectable()
export class PlanAcademicoService {    
       
    private mddlUrl = 'academicPlans';

    constructor(private http: HttpClient) { }

    get(): Observable<any> { 
        return this.http
        .get<any>(`${environment.base_url}/excelsis-api/${this.mddlUrl}/get`,{headers})
        .pipe(catchError(this.handleError));
    }

    create(data): Observable<any> { 
        return this.http
            .post<any>(`${environment.base_url}/excelsis-api/${this.mddlUrl}/create`,data,{headers})
            .pipe(catchError(this.handleError)
        );
    }

    uptate(data): Observable<any> { 
        return this.http
            .put<any>(`${environment.base_url}/excelsis-api/${this.mddlUrl}/update`,data,{headers})
            .pipe(catchError(this.handleError)
        );
    }

    delete(data): Observable<any> { 
        return this.http
            .delete<any>(`${environment.base_url}/excelsis-api/${this.mddlUrl}/delete`,data)
            .pipe(catchError(this.handleError)
        );
    }


    private handleError(err) {
        console.log('handleError ***',err);
        let errorMessage: string;
        if (err.error instanceof ErrorEvent) {
            errorMessage = `An error occurred: ${JSON.stringify((err.error && err.error.Message)|| err.message)}`;
        } else {
            errorMessage = `Backend returned error: ${JSON.stringify((err.error && err.error.Message) || err.message)}`;
        }
        return throwError(errorMessage);
    }
}
