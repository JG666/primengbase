import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { VisionXMdl } from '../models/vision.models';
const headers = new HttpHeaders().set("Content-Type", "application/json",);

@Injectable()
export class VisionService {
          
    url = 'vision';

    constructor(private http: HttpClient) { }

    proxy(data: VisionXMdl, mode:string) {
        switch (mode) {
            case 'new': return this.create(data);
            case 'edit': return this.update(data);
        } 
    } 

    get(): Observable<any> { 
        // return of(academicsDt);
        return this.http
        .get<any>(`${environment.base_url}/excelsis-api/${this.url}/get`,{headers})
        .pipe(catchError(this.handleError));
    }

    private create(data): Observable<any> { 
        return this.http
            .post<any>(`${environment.base_url}/excelsis-api/${this.url}/create`,data,{headers})
            .pipe(catchError(this.handleError)
        );
    }

    private update(data): Observable<any> { 
        return this.http
            .put<any>(`${environment.base_url}/excelsis-api/${this.url}/update`,data,{headers})
            .pipe(catchError(this.handleError)
        );
    }

    delete(data): Observable<any> { 
        return this.http
            .delete<any>(`${environment.base_url}/excelsis-api/${this.url}/delete/${data.id}`)
            .pipe(catchError(this.handleError)
        );
    }


    private handleError(err) {
        console.log('handleError ***',err);
        let errorMessage: string;
        if (err.error instanceof ErrorEvent) {
            errorMessage = `An error occurred: ${JSON.stringify((err.error && err.error.Message)|| err.message)}`;
        } else {
            errorMessage = `Backend returned error: ${JSON.stringify((err.error && err.error.Message) || err.message)}`;
        }
        return throwError(errorMessage);
    }
}

export const countriesMock = [{name :'Colombia', code:'COL'},{name :'Peru', code:'PR'},{name :'Argentina', code:'ARG'}];
export const institutionsMock = [{name :'Politecnico', code:'POL'},{name :'Uniminuto', code:'UNM'},{name :'Andina', code:'AND'}];

export const academicsDt = {
    "object": [
        {
            "id": 1,
            "name": "Para el 2017, la IUPG será reconocida en el ámbito nacional e internacional como la Institución de",
            "createDate": "2020-10-23T00:00:00.000+0000",
            "modify": "2020-10-23T00:00:00.000+0000",
            "status": true,
            "commentary": null,
            "institutionId": {
                "id": 3,
                "name": "Politecnico",
                "description": "Politecnico",
                "logo": null,
                "code": "001",
                "phone": "1234567",
                "address": "carrera 19 # 70-16",
                "typeInstitution": "Universidad",
                "createDate": "2020-10-14T00:00:00.000+0000",
                "modify": "2020-10-14T00:00:00.000+0000",
                "status": true,
                "commentary": null,
                "countries": {
                    "id": 1,
                    "name": "Colombia",
                    "description": "Colombia",
                    "formalDescription": "Colombia",
                    "createDate": "2020-07-10T00:00:00.000+0000",
                    "modify": "2020-07-10T00:00:00.000+0000",
                    "status": true,
                    "codeDane": "169",
                    "codePrefijo": "1",
                    "codePostal": null,
                    "codeCountry": "COL",
                    "commentary": null
                }
            }
        },
        {
            "id": 2,
            "name": "Formar excelentes seres humanos, profesionales competentes, éticamente orientados y comprometidos con la transformación social y el desarrollo sostenible.",
            "createDate": "2020-10-28T00:00:00.000+0000",
            "modify": "2020-10-28T00:00:00.000+0000",
            "status": true,
            "commentary": null,
            "institutionId": {
                "id": 4,
                "name": "Uniminuto",
                "description": "Universidad Minuto de Dios",
                "logo": null,
                "code": "UMD",
                "phone": "2918520",
                "address": "Carrera 73 # 83 - 50",
                "typeInstitution": "Universidad",
                "createDate": "2020-10-28T00:00:00.000+0000",
                "modify": "2020-10-28T00:00:00.000+0000",
                "status": true,
                "commentary": null,
                "countries": {
                    "id": 1,
                    "name": "Colombia",
                    "description": "Colombia",
                    "formalDescription": "Colombia",
                    "createDate": "2020-07-10T00:00:00.000+0000",
                    "modify": "2020-07-10T00:00:00.000+0000",
                    "status": true,
                    "codeDane": "169",
                    "codePrefijo": "1",
                    "codePostal": null,
                    "codeCountry": "COL",
                    "commentary": null
                }
            }
        },
        {
            "id": 3,
            "name": "Areandina será una Institución de",
            "createDate": "2020-10-28T00:00:00.000+0000",
            "modify": "2020-10-28T00:00:00.000+0000",
            "status": true,
            "commentary": "TOMADO DE LA PAGINA DE FUAA",
            "institutionId": {
                "id": 1,
                "name": "Andina",
                "description": "Andina",
                "logo": null,
                "code": "FUA",
                "phone": "1234567",
                "address": "carrera 19 # 70-16",
                "typeInstitution": "Universidad",
                "createDate": "2020-02-10T00:00:00.000+0000",
                "modify": "2020-02-10T00:00:00.000+0000",
                "status": true,
                "commentary": null,
                "countries": {
                    "id": 1,
                    "name": "Colombia",
                    "description": "Colombia",
                    "formalDescription": "Colombia",
                    "createDate": "2020-07-10T00:00:00.000+0000",
                    "modify": "2020-07-10T00:00:00.000+0000",
                    "status": true,
                    "codeDane": "169",
                    "codePrefijo": "1",
                    "codePostal": null,
                    "codeCountry": "COL",
                    "commentary": null
                }
            }
        }
    ],
    "token": "Bearer Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzb2Z0dGVrSldUIiwic3ViIjoid2lsc29uLmxvcGV6QG5lb3Jpcy5jb20iLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwiaWF0IjoxNjAzOTM0OTk5LCJleHAiOjE2MDM5MzU1OTl9.lCtU7sKb3wS9VXExJOcIojFNpwLseHC7gEiocrkbZQn7FXGY8HJKlF53xQoeKLyDzdLhq4luCKG5AyUbd7zf3g",
    "messages": "OK",
    "codError": "200"
};


    // getNotifyTest(url): Observable<any> { // claim:scring ~ module@feature  ex: salesMachineryCrm@feature
    // var headers = new HttpHeaders().set("Content-Type", "application/json",);
    // // httpClient.get('/status', { headers: { ignoreLoadingBar: '' } }); // ignore a particular $http GET loading bar:
    // return this.http
    //     .post<any>(`${environment.base_url}/api/${url}`,{headers})
    //     .pipe(
    //     map(dt => dt),
    //     catchError(this.handleError)
    //     );
    // }

    // getAuthTest(url, hdr?, claim?): Observable<any> { // claim:scring ~ module@feature  ex: salesMachineryCrm@feature
    // var headers = new HttpHeaders().set("Content-Type", "application/json",);
    // if(hdr) {
    //     headers =  new HttpHeaders({
    //         'Content-Type': 'application/json',
    //         'custom-access': claim });
    //     }
    // // httpClient.get('/status', { headers: { ignoreLoadingBar: '' } }); // ignore a particular $http GET loading bar:
    // console.log(`prms *** url : ${url} , claim : ${claim}`);
    // return this.http
    //     .get<any>(`${environment.base_url}/api/${url}`,{headers})
    //     .pipe(
    //     map(dt => dt),
    //     catchError(this.handleError)
    //     );
    // }