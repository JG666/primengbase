import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { VisionXMdl } from '../models/vision.models';
const headers = new HttpHeaders().set("Content-Type", "application/json",);

@Injectable()
export class MisionService {
          
    url = 'mision';

    constructor(private http: HttpClient) { }

    proxy(data: VisionXMdl, mode:string) {
        switch (mode) {
            case 'new': return this.create(data);
            case 'edit': return this.update(data);
        } 
    } 

    get(): Observable<any> { 
        return this.http
        .get<any>(`${environment.base_url}/excelsis-api/${this.url}/get`,{headers})
        .pipe(catchError(this.handleError));
    }

    private create(data): Observable<any> { 
        return this.http
            .post<any>(`${environment.base_url}/excelsis-api/${this.url}/create`,data,{headers})
            .pipe(catchError(this.handleError)
        );
    }

    private update(data): Observable<any> { 
        return this.http
            .put<any>(`${environment.base_url}/excelsis-api/${this.url}/update`,data,{headers})
            .pipe(catchError(this.handleError)
        );
    }

    delete(data): Observable<any> { 
        return this.http
            .delete<any>(`${environment.base_url}/excelsis-api/${this.url}/delete/${data.id}`)
            .pipe(catchError(this.handleError)
        );
    }

    private handleError(err) {
        console.log('handleError ***',err);
        let errorMessage: string;
        if (err.error instanceof ErrorEvent) {
            errorMessage = `An error occurred: ${JSON.stringify((err.error && err.error.Message)|| err.message)}`;
        } else {
            errorMessage = `Backend returned error: ${JSON.stringify((err.error && err.error.Message) || err.message)}`;
        }
        return throwError(errorMessage);
    }
}
