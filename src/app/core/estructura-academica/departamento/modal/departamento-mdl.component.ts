import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { VisionMdl, VisionXMdl } from '../../models/vision.models';
import { LocationService } from 'src/app/core/services/common/locations.service';
import { InstitutionService } from 'src/app/core/services/common/institutions.service';
import { MessageService } from 'primeng/api';
import { ConfigApp } from 'src/app/core/services/models/config-app.model';
import { DepartamentoService } from '../../services/departamento.service';

export class VisionData {
  mode: string;
  object
}
@Component({
  selector: 'app-departamento-mdl',
  templateUrl: './departamento-mdl.component.html',
  styleUrls: ['./departamento-mdl.component.css']
})
export class DepartamentoMdlComponent implements OnInit  {

  localDT: VisionData;
  formVision: FormGroup;
  visionObj: VisionMdl;
  institutionSelected: string;
  institutionQryRslt: any[];
  coutriesQryRslt: any[];
  submitted = false;
  es: any;
  tblData: ObjectArray[];
  clonedArray: { [s: string]: any; } = {};
  statuses = [
      {label: 'Contaduria Publica', value: 'Contaduria Publica'},
        {label: 'Ingenieria Electronica', value: 'Ingenieria Electronica'},
          {label: 'Lenguas Extrangeras', value: 'Lenguas Extrangeras'}]

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
              private dptoSvc:DepartamentoService, private fb: FormBuilder,
                private locationService:LocationService, private institutionService:InstitutionService,
                  private messageService: MessageService,) {
    this.localDT = this.config.data;
    console.log('data recieved from parent ***', this.localDT);
  }

    ngOnInit() {
      this.formVision = this.fb.group({
        description : ['', Validators.required],
        institution : ['', Validators.required],
        country : ['', Validators.required],
        status : [false, Validators.required],
      });

      if(this.localDT.mode === 'edit') {
        const dt = {...this.localDT.object};
        const x = {
          id: dt.id,
          description: dt.name,
          country: {name : dt.institutionId.countries.name, code: dt.institutionId.countries.name}, 
          institution: {name : dt.institutionId.name, code: dt.institutionId.name},
          status: dt.status,
        } ;
        this.formVision.patchValue({...x});
      }

      
          this.tblData =  [
            {
              'id' : '1000',
              'code' : 'Contaduria Publica',
              'name' : '100%',
            }
          ]

    }


    onDateSelect(e,op) {
      // switch (op) {
      //     case 'init':
      //         this.fechas.fechaInicial = e;
      //         this.table.filter(this.utils.getUtcFormated(this.fechas.fechaInicial,'YYYY-MM-DD'), 'fechaCrea', 'gte');
      //     break;
      //         case 'end':
      //             if(!!this.fechas.fechaInicial) {
      //             this.fechas.fechaFinal = e;
      //             this.table.filter(this.utils.getUtcFormated(e,'YYYY-MM-DD'), 'fechaCrea', 'lte');
      //             console.log(this.fechas )
      //         }
      //     break;
      // }
  }

  search(dt, op) {
    switch (op) {
      case 'country':
        this.coutriesQryRslt = this.locationService.getStoreOp('countries').filter(d => d.name.toLowerCase().includes(dt.query.toLowerCase()));
      break;
      case 'institution':
        this.institutionQryRslt = this.institutionService.getStoreOp('institutions').filter(d => d.name.toLowerCase().includes(dt.query.toLowerCase()));
      break;
    }
  }

  handleDropdown(event) {
      //event.query = current value in input field
  }

  hideDialog() {
    this.submitted = false;
    this.ref.close({mode:null, object: null } as VisionData);
  }
  
  saveObject() {
      this.submitted = true;
      if (this.formVision.valid) {
        const objValues = this.formVision.value;
          if (this.localDT.mode === 'edit') {
            const dtToEdit = {...this.localDT.object};
            const xDt = {
              id: dtToEdit.id, description:  objValues.description,
              country:  objValues.country.id,
              institution:   objValues.institution.id,
              status:   objValues.status,
            };
            this.onCreateVision(xDt);
          }
          else {
            const xDt = {
              id: objValues.id, description:  objValues.description,
              country:  objValues.country.id,
              institution:   objValues.institution.id,
              status:   objValues.status,
            };
            this.onCreateVision(xDt);
          }
      }
  }


  //#region handle CRUD Vision
    
  onCreateVision(rslt) {    
    this.dptoSvc.create(rslt).subscribe(rs => {
      console.log('result create vision ***', rs);
      this.resetModalObj({});
      this.messageService.add({severity:'success', summary: 'Data saving', detail: 'item....'});
    }, 
    (err) => {
      console.log('result create vision ***', err);
      this.messageService.add({severity:'error', summary: 'Data saving', detail: 'item....'});
    });
  }

  resetModalObj(dt:any) {
    this.formVision.reset();
    this.submitted = false;
    setTimeout(() => {
      this.ref.close({mode: this.localDT.mode, object: dt } as VisionData);
    }, 200);
  }

//#endregion

  //#region inline edition

  addTmplNewRow() {
    this.clonedArray['newBackUp'] = [...this.tblData];
    const newId = Math.random().toString(36).substring(2);
    const obj = { id: newId , code: '', name: ''};
    this.clonedArray[newId]  =  obj;
    return obj;
  }

  onRowEditInit(row: any) {
    console.log('onRowEditInit', row);
      this.clonedArray[row.id] = {...row};
  }

  onRowEditSave(row: any) {
    console.log('onRowEditSave', row);
      if ((row.name).trim().length) {
          delete this.clonedArray[row.id];
          this.messageService.add({key:'mdl-toast',severity:'success', summary: 'Success', detail:'Row is updated'});
      }  
      else {
          this.messageService.add({key:'mdl-toast',severity:'error', summary: 'Error', detail:'Invalid value'});
      }
  }

  onRowEditCancel(row: any, index: number) {
    console.log('onRowEditCancel', row,index);
      // this.tblData = [...this.clonedArray['newBackUp']];
      this.tblData = this.tblData.filter(r => r.id !== row.id)
      // this.tblData[index] = this.clonedArray[row.id];
      delete this.clonedArray['newBackUp'];
  }

  //#endregion

  // clonedProducts: { [s: string]: any; } = {};

  // products2: any[] = [
  //   {
  //     "id": "1000",
  //     "code": "f230fh0g3",
  //     "name": "Bamboo Watch",
  //     "description": "Product Description",
  //     "image": "bamboo-watch.jpg",
  //     "price": 65,
  //     "category": "Accessories",
  //     "quantity": 24,
  //     "inventoryStatus": "INSTOCK",
  //     "rating": 5
  //   },
  //   {
  //     "id": "1001",
  //     "code": "nvklal433",
  //     "name": "Black Watch",
  //     "description": "Product Description",
  //     "image": "black-watch.jpg",
  //     "price": 72,
  //     "category": "Accessories",
  //     "quantity": 61,
  //     "inventoryStatus": "INSTOCK",
  //     "rating": 4
  //   }];

  //   onRowEditInit(product: any) {
  //     this.clonedProducts[product.id] = {...product};
  // }

  // onRowEditSave(product: any) {
  //     if (product.price > 0) {
  //         delete this.clonedProducts[product.id];
  //         this.messageService.add({severity:'success', summary: 'Success', detail:'Product is updated'});
  //     }  
  //     else {
  //         this.messageService.add({severity:'error', summary: 'Error', detail:'Invalid Price'});
  //     }
  // }

  // onRowEditCancel(product: any, index: number) {
  //     this.products2[index] = this.clonedProducts[product.id];
  //     delete this.products2[product.id];
  // }

}

export interface ObjectArray {
  id?:string;
  code?:string;
  name?:string;
}

export const tblData = [
  {
    id : '1000',
    code : 'f230fh0g3',
    name : '100%',
  },
  {
    id : '1001',
    code : 'nvklal433',
    name : '30%',
  },
  {
    id : '1002',
    code : 'nvklal433',
    name : '40%',
  },
  {
    id : '1003',
    code : 'nvklal433',
    name : '50%',
  },
];