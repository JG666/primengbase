
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/services/Auth/auth-guard.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { MomentDateService } from 'src/app/services/moment.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { DepartamentoMdlComponent } from './modal/departamento-mdl.component';
import { DialogService } from 'primeng/dynamicdialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocationService } from '../../services/common/locations.service';
import { InstitutionService } from '../../services/common/institutions.service';
import { DepartamentoComponent } from './departamento.component';
import { DepartamentoService } from '../services/departamento.service';
import { AddRowDirective } from 'src/app/shared/directive/add-row.directive';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '',canActivate: [AuthGuardService], component: DepartamentoComponent }]),
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [DepartamentoComponent,DepartamentoMdlComponent,AddRowDirective],
  providers: [DepartamentoService, MomentDateService, MessageService, ConfirmationService,DialogService,
                LocationService,InstitutionService],
})
export class DepartamentoModule { }
