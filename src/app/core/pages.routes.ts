import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { StartComponent } from './start/start.component';
import { AuthGuardService } from '../services/Auth/auth-guard.service';

const pagesRoutes : Routes = [
    {
        path: '', component: PagesComponent,
        canActivate: [AuthGuardService],
        children: [
            { path: '', redirectTo: '/inicio', pathMatch: 'full'},
            { path: 'inicio', canActivate: [AuthGuardService], component : StartComponent},
            { path: 'md-test/ft-test', canActivate: [AuthGuardService], loadChildren: () => import('./form-test/form-test.module').then(m => m.FormTestModule) },
            { path: 'md-configuraciongeneral/ft-componentes', canActivate: [AuthGuardService], loadChildren: () => import('./configuracion-general/components/components.module').then(m => m.ComponentsModule) },
            { path: 'md-configuraciongeneral/ft-permisos', canActivate: [AuthGuardService], loadChildren: () => import('./configuracion-general/access/access.module').then(m => m.AccessModule) },
            { path: 'md-configuraciongeneral/ft-roles', canActivate: [AuthGuardService], loadChildren: () => import('./configuracion-general/roles/roles.module').then(m => m.RolesModule) },
            { path: 'md-configuraciongeneral/ft-componentes', canActivate: [AuthGuardService], loadChildren: () => import('./configuracion-general/users/users.module').then(m => m.UsersModule) },
            { path: 'md-estructuraacademica/ft-mision', canActivate: [AuthGuardService], loadChildren: () => import('./estructura-academica/mision/mision.module').then(m => m.MisionModule) },
            { path: 'md-estructuraacademica/ft-vision', canActivate: [AuthGuardService], loadChildren: () => import('./estructura-academica/vision/vision.module').then(m => m.VisionModule) },
            { path: 'md-estructuraacademica/ft-planacademico', canActivate: [AuthGuardService], loadChildren: () => import('./estructura-academica/plan-academico/plan-academico.module').then(m => m.PlanAcademicoModule) },
            { path: 'md-estructuraacademica/ft-departamento', canActivate: [AuthGuardService], loadChildren: () => import('./estructura-academica/departamento/departamento.module').then(m => m.DepartamentoModule) },
            { path: 'md-estructuraacademica/ft-lineaaccion', canActivate: [AuthGuardService], loadChildren: () => import('./estructura-academica/linea-accion/linea-accion.module').then(m => m.LineaAccionModule) },
            { path: 'md-estructuraacademica/ft-programaacademico', canActivate: [AuthGuardService], loadChildren: () => import('./estructura-academica/programa-academico/programa-academico.module').then(m => m.ProgramaAcademicoModule) },
        ]
    },       
    // { path: '', redirectTo: '/inicio', pathMatch: 'full'},
    { path: '**', redirectTo: '/inicio', pathMatch: 'full'},
];

export const PAGES_ROUTES  = RouterModule.forChild(pagesRoutes);
