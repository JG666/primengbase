import {NgModule,CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA} from "@angular/core";
import { CommonModule } from "@angular/common";

import { RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/services/Auth/auth-guard.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormTestComponent } from './form-test.component';
import { TestService } from '../../services/test.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{ path: '',canActivate: [AuthGuardService], component: FormTestComponent }])
  ],
  declarations: [FormTestComponent],
  providers: [TestService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class FormTestModule {}
