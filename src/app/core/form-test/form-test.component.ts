import { Component, OnInit } from '@angular/core';
import { TestService } from 'src/app/services/test.service';
import { MessageService } from 'primeng/api';



@Component({
  selector: "app-form-test",
  templateUrl: "./form-test.component.html",
  styleUrls: ["./form-test.component.css"],
})
export class FormTestComponent implements OnInit {

  constructor(private testService: TestService, private messageService: MessageService) {}

  ngOnInit() {}

  
  checkNtfTest(op:string) {
    this.messageService.clear();
     this.testService
      .getNtfTesting(op)
      .subscribe( dt  => {
          this.addSingle('success',"success ");
          this.clear();
        },
        (err) => {
          console.log('checkNtfTest error *** ',err);
          this.addSingle('error',"error"); 
          this.clear();         
        },
      );

  }
  checkNotifyTest(op:string) {
    this.messageService.clear();
     this.testService
      .getNotifyTest(op)
      .subscribe( dt  => {
          console.log('checkNotifyTest success *** ',dt);          
          this.addSingle('success',"success ");
          this.clear();
        },
        (err) => {
          console.log('checkNotifyTest error *** ',err);
          this.addSingle('error',"error"); 
          this.clear();         
        },
      );

  }

  raiseError() {
    throw new Error("Oops something went wrong");
  }

  checkAuth(op:string) {
    let claim;
    if(op.includes('policy')){
      claim = 'md-test@ft-test';
    }
    this.messageService.clear();
     this.testService
      .getAuthTest(op,!!claim,claim)
      .subscribe((dt) => {
          console.log('success *** ',dt);
          this.addSingle('success',"success ");
          this.clear();
        },
        (err) => {
          console.log(' checkAuth error *** ',err);
          this.addSingle('error',"error"); 
          this.clear();         
        },
      );

  }

  addSingle(svrt:string,dtls:string) {
      this.messageService.add({severity: svrt, summary:'Result...', detail: dtls});
  }

  addMultiple() {
      this.messageService.addAll([{severity:'success', summary:'Result...', detail:'Via MessageService'},
                      {severity:'info', summary:'Info Message', detail:'Via MessageService'}]);
  }
  
  clear() {
    setTimeout(() => {
      this.messageService.clear();
    }, 5000);
  }


}
