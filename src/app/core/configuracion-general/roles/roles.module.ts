import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/services/Auth/auth-guard.service';
import { RolesComponent } from './roles.component';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';



@NgModule({
  imports: [
    TableModule,
    ButtonModule,
    CommonModule,
    RouterModule.forChild([{ path: '',canActivate: [AuthGuardService], component: RolesComponent }])
  ],
  declarations: [RolesComponent]
})
export class RolesModule { }
