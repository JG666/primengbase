import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccessComponent } from './access.component';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/services/Auth/auth-guard.service';


@NgModule({
  imports: [
    TableModule,
    ButtonModule,
    CommonModule,
    RouterModule.forChild([{ path: '',canActivate: [AuthGuardService], component: AccessComponent }])
  ],
  declarations: [AccessComponent]
})
export class AccessModule { }
