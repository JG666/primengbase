import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  private urlAPI = 'user/get';
  constructor(private http: HttpClient) {
    let status : boolean = true;
    status = false;
  }
  
  ngOnInit(){

  }

  getAllrole():Observable<any>{
    return this.http.get<any>(`${this.urlAPI}`, {responseType: 'json'})
  }



}
