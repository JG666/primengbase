export interface ComponentS {
  id: string;
  name: string;
  description: string;
  type: string;
  createDate: string;
  modify: string;
  status: string;
  url: null;
}
