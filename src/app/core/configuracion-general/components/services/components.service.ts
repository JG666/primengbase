import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';


const headers = new HttpHeaders().set("Content-Type", "text/plain",);

@Injectable()
export class ComponentsService {

  constructor(private http: HttpClient) {
    let status : boolean = true;
    status = false;
   }


   getComponents(): Observable<any> { 
    /* return of(academicsDt); */
    return this.http
         .get<any>(`${environment.base_url}/excelsis-api/components/get`,{headers})
         .pipe(catchError(this.handleError));
  }

  delComponents(id:number): Observable<any> {
    return this.http
         .delete<any>(`${environment.base_url}/excelsis-api/components/delete/${id}`,{headers})
         .pipe(catchError(this.handleError));
  }

  private handleError(err) {
    console.log('handleError ***',err);
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        errorMessage = `An error occurred: ${JSON.stringify((err.error && err.error.Message)|| err.message)}`;
    } else {
        // The backend returned an unsuccessful response code. The response body may contain clues as to what went wrong,
        errorMessage = `Backend returned error: ${JSON.stringify((err.error && err.error.Message) || err.message)}`;
    }
    return throwError(errorMessage);
}


 /*  getAllcomponents():Observable<componentS[]>{
    return this.http.get<componentS[]>();
  }

  handleError<T>(arg0: string): (err: any, caught: Observable<componentS>) => import("rxjs").ObservableInput<any> {
    throw new Error("Method not implemented.");
  }
  log(arg0: string): void {
    throw new Error("Method not implemented.");
  } */
}
