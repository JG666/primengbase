import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Table } from 'primeng/table';
import { SubSink } from 'subsink';
import { ComponentsService } from './services/components.service';

@Component({
  selector: 'app-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.css']
})
export class ComponentsComponent implements OnInit {

  private subSink = new SubSink();
  academicData:any[];
  @ViewChild('tblVision') table: Table;
  selectItemVsn: any[];

  ref: DynamicDialogRef;

  constructor(private componentsService: ComponentsService,
              public dialogService: DialogService,
              private messageService: MessageService,
              private confirmationService: ConfirmationService) {}

  ngOnInit() {
    this.subSink.sink =  this.componentsService.getComponents().subscribe(rs => {
      const {object} = rs
      this.academicData = object.map(o => { return {...o, createDate: (o['createDate']).split('T')[0], modify: (o['modify']).split('T')[0] }});
      console.log('getComponents list***',this.academicData);
    });
  }

  deleteSelectItemVsn(index: number) {
    console.log('deleteSelectItemVsn***');
    this.confirmationService.confirm({
        message: 'Esta seguro de eliminar este registro?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
            this.academicData = this.academicData.filter(val => !this.selectItemVsn.includes(val));
            console.log(this.academicData);
            this.selectItemVsn = null;
            this.messageService.add({severity:'success', summary: 'Successful', detail: 'Registro Eliminado', life: 3000});
        }
    });
  }

 /*  openNew() {
    this.ref = this.dialogService.open(, {
        data: {
          customDT: 'Titulo del modal'
        },
        header: 'Choose a Product',
        width: '70%',
        contentStyle: {"max-height": "500px", "overflow": "auto"},
        baseZIndex: 10000
    });
  
    this.ref.onClose.subscribe((item) =>{
        this.messageService.add({severity:'info', summary: 'Product Selected', detail: 'item'});
    });
  
  } */
  
    ngOnDestroy(): void {
      if (this.ref) {
          this.ref.close();
      }
      this.subSink.unsubscribe();
    }
  


}
