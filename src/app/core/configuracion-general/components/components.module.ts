import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsComponent } from './components.component';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/services/Auth/auth-guard.service';
import { PrimegnModule } from 'src/app/common/primegn/primegn.module';
import { ComponentsService } from './services/components.service';
import { MomentDateService } from 'src/app/services/moment.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';



@NgModule({
  imports: [
    PrimegnModule,
    CommonModule,
    RouterModule.forChild([{ path: '',canActivate: [AuthGuardService], component: ComponentsComponent }])
  ],
  declarations: [ComponentsComponent],
  providers: [ComponentsService, MomentDateService, MessageService, ConfirmationService,DialogService],
})
export class ComponentsModule { }
