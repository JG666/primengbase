import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { PAGES_ROUTES } from './pages.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StartModule } from './start/start.module';
import { MenuAppModule } from '../common/menu/menu.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    PAGES_ROUTES,
    MenuAppModule,       
    FormsModule,
    ReactiveFormsModule,
    StartModule,
    SharedModule,
    
],
declarations: [
    PagesComponent
],
exports: [
    PagesComponent,
    FormsModule,
],   
providers :[],
schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class PagesModule { }
